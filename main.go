package main

import (
	"fmt"
	"os"

	"gitlab.com/Misakey/auth-backend/src/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
