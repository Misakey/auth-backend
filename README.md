# Introduction
auth-backend, also known as MK Auth is responsible for managing user accounts authorization and authentication.
So this project is responsible for Authentication/Authorization's business domain:
 - stores and manages user credentiales (register, account confirmation, password changes, 2FA authentication...)
 - it has some routes interacting with Hydra (an authorization tool we use to handle oAuth2 and Open ID Connect protocols).

------

## Table of content

* [Introduction](#introduction)
* [Documentation](#documentation)
  * [API Documentation](#api-documentation)
* [Dependencies](#dependencies)
  * [Docker](#docker)
  * [Hydra](#hydra)
* [Folder Architecture](#folder-architecture)
* [Configuration](#configuration)
  * [Environment](#environment)
  * [Configuration File](#configuration-file)
* [Database Migrations](#database-migrations)
  * [Migrate Your Database](#migrate-your-database)
  * [Create Migrations](#create-migrations)
  * [Models generation with SQLBoiler](#models-generation-with-sqlboiler)
* [Cleanup Job](#cleanup-job)
* [Deployment](#deployment)

------

## Documentations

### API Documentation

An API documentation is available as tools to built it. More information [here](./docs/swagger/README.md).

------

## Dependencies
#### Docker

The project is using [docker](https://www.docker.com/community-edition) to facilitate running and deployment.

#### Hydra

This service interacts with a running [Ory Hydra](https://github.com/ory/hydra) which is an OpenID Connect certified OAuth2 Server.
Hydra allows us to not handle Open ID Connect & OAuth2 protocol on our side and offers set of features to make it scalable and secure.

In order to perform authentication requests you need to setup Hydra.
Hydra serves an API which allows us to Create/Update/Get/Remove OAuth2 clients.

------

## Folder architecture

_Main folders:_
- `docs`: documentation resources, API documentation, ...
- `config`: service config example and sqlboiler configuration.
- `src`: source code of the project.

_Concerning the source code:_

The service aims to follow Clean Architecture principles ([here is a quick introduction](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)), and Layered Architecture.
Here is the list of folders and a description:
- `cmd`: possible commands.
- `adaptor`: code linked to frameworks and drivers we use to build our service, mostly for customisation purpose.
- `controller`: presenters layers, handling data transformation (usually from JSON to internal model).
- `model`: our model, understandable and used by all our layers.
- `repo`: handle retrieval/storage of models (can use a DB, an API...).
- `service`: most of the business logic, interact with repo to play with models/data.
- `db`: files for database migration.
------

## Configuration

Configuration is done on **2 levels**:
- [some mandatory environment variables](#environment) (used mostly for credentials)
- [a configuration file](#configuration-file) (we use [viper](https://github.com/spf13/viper#what-is-viper) so many possibilities in term of format)

#### Environment

Environment variables are needed for some internal libraries. We also store secret in environment today.

Here is the list of variable to set up to have the auth service working properly:

docker-compose.yml setup:
- `DATABASE_URL`: the [postgres URL](https://jdbc.postgresql.org/documentation/80/connect.html) to connect to the database.
- `ENV`: the environment mode you choose (`production` or `development`):
  - Print emails on os.Stdout instead of using Amazon SES.
  - Use of local storage instead of Amazon S3.
  - Use a different [configuration file](#configuration-file).
- `AWS_ACCESS_KEY` (only if `ENV=production`): [Access key for AWS](https://docs.aws.amazon.com/sdk-for-go/api/aws/credentials/).
- `AWS_SECRET_KEY` (only if `ENV=production`): [Secret key for AWS](https://docs.aws.amazon.com/sdk-for-go/api/aws/credentials/).

#### Configuration File

The service always tries to read the config file from the `/etc` directly.

In development mode, it tries to read `auth-config.dev.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is automatically added to the built Docker image using `config/auth-config.toml`.

An example a of configuration file (using TOML) is available in `config` folder.
In production mode, it tries to read `auth-config.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is **NOT** automatically added to the built Docker image. A volume must be mounted to share it.

An example a of configuration file (using TOML) is available in the `config` folder.


------

## Database Migrations

Database Migrations are handled by [Pressly Goose](https://github.com/pressly/goose) which is a fork improving [Liamstask Goose](https://bitbucket.org/liamstask/goose).

The migration tool is encapsulated in a Docker container.

You also need to install SQLBoiler to generate models after database migrations.

:warning: Always use `src/db/docker-compose.yml` to run database & migrate tool while creating migrations.

#### Migrate Your Database

To migrate the database you just have to launch the `auth_backend` container which run automatically the `auth_migrate` and `auth_db` one.

The default command sent to goose is `up` which apply all missing migrations to your running database.
You can override this command by setting [docker-compose command](https://docs.docker.com/compose/compose-file/compose-file-v2/#command).

The following syntax would perform a `goose down` instead of the default `goose up`.

```yaml
  auth_migrate:
    command: "migrate --goose=down"
```

#### Create Migrations

:warning: Always use `src/db/docker-compose.yml` to run database & migrate tool while creating migrations.

See [goose usage](https://github.com/pressly/goose#usage) to create migrations.

You must use `goose` locally to create migrations, since the migration file must created on your local storage and not inside a container, it is more practical.
So basically do not use Docker to perform this action :).

A hint of what could look your migration creation command line:

`goose -dir=./src/db/migration/ postgres "postgres://misakey:secret@localhost:5433/auth?sslmode=disable" create add_user_display_name_column`

#### Models generation with SQLBoiler

:warning: Always use `src/db/docker-compose.yml` to run database & migrate tool while creating migrations.

If you need to alter the SQL database scheme, you'll need to install [sqlboiler](https://github.com/volatiletech/sqlboiler#download) which is the ORM we use.

SQLBoiler is a scheme based ORM which generates models according to SQL scheme in order to optimize queries.

You can regenerate models using the following command, more information [here](https://github.com/volatiletech/sqlboiler#initial-generation):

`sqlboiler psql --wipe --config config/sqlboiler.toml --pkgname model`

:warning: As SQLBoiler builds its models by reading a database, you need to have a running database with all migrations **properly** performed. You can just run `docker-compose up -d` in the `src/db` folder to run such a database.

SQLBoiler generates by default a `models` folder, so you must move generated files and replace existing one with the new versions.
:warning: We advise to not use `--output=src/model` option since model files unrelated to sqlboiler would be removed.
:warning: Be careful, all existing files will be overriden inside this directory, so be aware of struct tags extension that have been put in place on top of sqlboiler models for example !

## Cleanup Job

The `auth-backend` binary includes a cleanup job.

You can use it by running test-and-run and launching:

```
docker run --network test-and-run_misakey_vpn -e "ENV=development" -e "DATABASE_URL=postgres://misakey:secret@auth_db:5432/auth?sslmode=disable" registry.gitlab.com/misakey/auth-backend:<YOUR TAG> cleanup
```

It is intended to be used as a cronjob and takes the same configuration file as the main command.

Its specific configuration lies in the `[cleanup]` section.

It also needs the `ENV` and the `DATABASE_URL` environment variables.

It will scan the db and hard deletes objects that need to be deleted.

## Deployment

:warning: Make sure the corresponding images exist before running the following commands.

Create a `config.yaml` file with a `config:` key and the `config.toml` production content.


When you want to deploy the application for the first time, clone the repo, checkout to the desired tag, then run:

```
helm install --name auth-backend helm/auth-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest

```

If you just need to upgrade the application, then run:
```
helm upgrade auth-backend helm/auth-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest
```

The command will hang during the migration. Check in another terminal if there is no problem with the migration.

If the migration fails, CTRL+C your helm command, then run `kubectl delete jobs auth-backend-migrate`, then solve the problem and re-run the helm command.
