package cmd

import (
	"context"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/config"
	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/auth-backend/src/adaptor/sql"
	"gitlab.com/Misakey/auth-backend/src/repo"
	"gitlab.com/Misakey/auth-backend/src/service"
)

var cleanupCmd = &cobra.Command{
	Use:   "cleanup",
	Short: "Cleanup some resources",
	Long: `Hard delete resources marked as to_delete when a specific number of days
	has passed since the soft deletion. This number of days is defined in the cleanup section
	of the configuration file.`,
	Run: func(cmd *cobra.Command, args []string) {
		runCleanup()
	},
}

func init() {
	RootCmd.AddCommand(cleanupCmd)
}

func runCleanup() {
	// init logger
	zerologger := logger.ZerologLogger()
	log.Logger = zerologger
	ctx := context.Background()
	ctx = context.WithValue(ctx, "logger", &zerologger)

	// TODO: init config
	viper.AddConfigPath("/etc/")

	// handle missing mandatory fields
	mandatoryFields := []string{
		"mail.templates",
		"cleanup.time_before_deletion",
	}

	// handle envs
	switch env {
	case "production":
		viper.SetConfigName("auth-config")
		mandatoryFields = append(mandatoryFields, []string{"aws.ses_region"}...)
		if os.Getenv("AWS_ACCESS_KEY") == "" {
			log.Warn().Msg("AWS_ACCESS_KEY not set")
		}
		if os.Getenv("AWS_SECRET_KEY") == "" {
			log.Warn().Msg("AWS_SECRET_KEY not set")
		}
	case "development":
		viper.SetConfigName("auth-config.dev")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)

	// init db connections
	dbConn, err := sql.NewPostgreConn(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal().Err(err).Msg("could not connect to db")
	}

	userAccountRepo := repo.NewUserAccountPSQL(dbConn)
	userDeletionRepo := repo.NewUserDeletionSQLBoiler(dbConn)
	mailerRepo := getMailer()
	emailTemplateRepo := repo.NewTemplateFileSystem(viper.GetString("mail.templates"))
	emailRenderer, err := service.NewEmailRenderer(
		emailTemplateRepo,
		[]string{"confirm_html", "confirm_txt",
			"reset_txt", "reset_html",
			"delete_html", "delete_txt",
			"deletion_reminder_html", "deletion_reminder_txt"},
		viper.GetString("mail.from"),
	)
	if err != nil {
		log.Fatal().Err(err).Msg("email renderer")
	}

	cleanupManager := service.NewCleanupManager(
		emailRenderer, mailerRepo, userAccountRepo,
		userDeletionRepo, viper.GetDuration("cleanup.time_before_deletion"),
	)

	err = cleanupManager.CleanupAccounts(ctx)
	if err != nil {
		log.Fatal().Err(err).Msg("cleanup account failed")
	}
	log.Info().Msg("cleanup job successfull")
}
