package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/auth"
	"gitlab.com/Misakey/msk-sdk-go/bubble"
	"gitlab.com/Misakey/msk-sdk-go/config"
	mecho "gitlab.com/Misakey/msk-sdk-go/echo"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	mhttp "gitlab.com/Misakey/msk-sdk-go/rester/http"

	ahttp "gitlab.com/Misakey/auth-backend/src/adaptor/rester/http"
	"gitlab.com/Misakey/auth-backend/src/adaptor/sql"
	"gitlab.com/Misakey/auth-backend/src/controller"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/auth-backend/src/repo"
	"gitlab.com/Misakey/auth-backend/src/service"
)

var cfgFile string
var goose string
var env = os.Getenv("ENV")

func init() {
	cobra.OnInitialize()
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	RootCmd.PersistentFlags().StringVar(&goose, "goose", "up", "goose command")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

var RootCmd = &cobra.Command{
	Use:   "auth",
	Short: "Run the auth service",
	Long:  `This service is responsible for managing authorization & authentication of our system`,
	Run: func(cmd *cobra.Command, args []string) {
		initService()
	},
}

func initService() {
	// init logger
	log.Logger = logger.ZerologLogger()

	// add error needles to auto handle some specific errors on layers we use everywhere
	bubble.AddNeedle(bubble.PSQLNeedle{})
	bubble.AddNeedle(bubble.ValidatorNeedle{})
	bubble.AddNeedle(bubble.EchoNeedle{})
	bubble.Lock()

	initDefaultConfig()

	// init echo framework with compressed HTTP responses, custom logger format and custom validator
	e := echo.New()
	e.Use(mecho.NewZerologLogger())
	e.Use(mecho.NewLogger())
	e.Use(mecho.NewCORS())
	e.Use(middleware.Recover())

	e.Validator = mecho.NewValidator()
	e.HTTPErrorHandler = mecho.Error
	e.HideBanner = true

	// init auth middleware
	jwtValidator := mecho.NewJWTMidlw(true)

	// init db connections
	dbConn, err := sql.NewPostgreConn(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal().Err(err).Msg("could not connect to db")
	}

	// init resters
	publicHydraRester := mhttp.NewClient(viper.GetString("hydra.public_endpoint"), viper.GetBool("hydra.secure"))
	formPublicHydraRester := ahttp.NewPostFormClient(viper.GetString("hydra.public_endpoint"), viper.GetBool("hydra.secure"))
	adminHydraRester := mhttp.NewClient(viper.GetString("hydra.admin_endpoint"), viper.GetBool("hydra.secure"))
	formAdminHydraRester := ahttp.NewPostFormClient(viper.GetString("hydra.admin_endpoint"), viper.GetBool("hydra.secure"))
	feedbackRester := mhttp.NewClient(viper.GetString("feedback.endpoint"), viper.GetBool("feedback.secure"))
	applicationRester := mhttp.NewClient(viper.GetString("application.endpoint"), viper.GetBool("application.secure"))
	databoxRester := mhttp.NewClient(viper.GetString("databox.endpoint"), viper.GetBool("databox.secure"))
	consentRester := mhttp.NewClient(viper.GetString("consent.endpoint"), viper.GetBool("consent.secure"))

	// init repositories
	avatarRepo := getAvatarRepo()
	customAccessRepo := repo.NewCustomAccessPSQL(dbConn)
	emailTemplateRepo := repo.NewTemplateFileSystem(viper.GetString("mail.templates"))
	hydraRepo := repo.NewHydraHTTP(publicHydraRester, formPublicHydraRester, adminHydraRester, formAdminHydraRester)
	mailerRepo := getMailer()
	userAccountRepo := repo.NewUserAccountPSQL(dbConn)
	backupRepo := repo.NewBackupPSQL(dbConn)
	publicKeyRepo := repo.NewPublicKey(dbConn)
	applicationFeedbackRepo := repo.NewApplicationFeedbackHTTP(feedbackRester)
	applicationInfoRepo := repo.NewApplicationInfoHTTP(applicationRester)
	databoxRepo := repo.NewDataboxHTTP(databoxRester)
	userRoleRepo := repo.NewUserRolePSQL(dbConn)
	userConsentRepo := repo.NewUserConsentHTTP(consentRester)
	consentRepo := repo.NewConsentHTTP(consentRester)
	userDeletionRepo := repo.NewUserDeletionSQLBoiler(dbConn)

	appAuthService := auth.NewAuthorizationCodeFlow(
		viper.GetString("app.client_id"),
		viper.GetString("app.client_secret"),
		viper.GetString("auth.auth_url"),
		viper.GetString("auth.token_url"),
		viper.GetString("app.redirect_code_url"),
		viper.GetString("app.redirect_token_url"),
	)
	misadminAuthService := auth.NewAuthorizationCodeFlow(
		viper.GetString("misadmin.client_id"),
		viper.GetString("misadmin.client_secret"),
		viper.GetString("auth.auth_url"),
		viper.GetString("auth.token_url"),
		viper.GetString("misadmin.redirect_code_url"),
		viper.GetString("misadmin.redirect_token_url"),
	)

	emailRenderer, err := service.NewEmailRenderer(
		emailTemplateRepo,
		[]string{"confirm_html", "confirm_txt",
			"reset_txt", "reset_html",
			"delete_html", "delete_txt",
			"deletion_reminder_html", "deletion_reminder_txt"},
		viper.GetString("mail.from"),
	)
	if err != nil {
		log.Fatal().Err(err).Msg("email renderer")
	}

	ssoClientService := service.NewSSOClientManager(hydraRepo, applicationInfoRepo,
		viper.GetString("app.client_id"),
		viper.GetString("misadmin.client_id"))
	authManager := service.NewAuthManager(
		emailRenderer, mailerRepo,
		userAccountRepo, avatarRepo, backupRepo, publicKeyRepo,
		hydraRepo, hydraRepo,
		customAccessRepo, userRoleRepo, userConsentRepo,
		applicationFeedbackRepo, databoxRepo, consentRepo, userDeletionRepo,
		viper.GetString("auth.login_url"),
		viper.GetString("application.client_id"), viper.GetString("application.client_secret"),
		viper.GetString("misadmin.client_id"), viper.GetString("misadmin.client_secret"),
		viper.GetDuration("cleanup.time_before_deletion"),
	)
	// init controllers
	userRoleController := controller.NewUserRoleEcho(authManager)
	tokenController := controller.NewAuthTokenEcho(authManager)
	loginController := controller.NewLoginEcho(authManager)
	userAccountController := controller.NewUserAccountEcho(authManager)
	appAuthController := controller.NewAuthFlowEcho(appAuthService, "")
	misadminAuthController := controller.NewAuthFlowEcho(misadminAuthService, "misadmin")
	ssoClientController := controller.NewSSOClientEcho(ssoClientService)

	// Init generic
	genericController := controller.NewGeneric()

	// bind routes to controllers:

	// ~ Routes linked to authorization from internal network ~
	auth := e.Group("auth")
	// custom, temporary & anonymous tokens handling for some restricted routes
	auth.POST("/custom-access", tokenController.CreateCustomAccess, jwtValidator)
	// route to verify authentication from gateway
	auth.GET("", tokenController.Auth)
	e.GET("/auth-optional", tokenController.AuthOptional)
	e.GET("/auth-custom", tokenController.AuthCustom)

	// ~ Routes linked to authorization from internal network ~
	// - routes to authenticate/authorize mkapp client
	appAuth := e.Group("app")
	appAuth.GET("/auth/callback", appAuthController.ExchangeToken)

	// - routes to authenticate/authorize misadmin client
	authMisadmin := e.Group("misadmin")
	authMisadmin.GET("/auth", misadminAuthController.RequestCode)
	authMisadmin.GET("/auth/callback", misadminAuthController.ExchangeToken)

	// ~ Routes linked to authorization/authentication from external network ~
	// - login flow
	login := e.Group("login")
	login.GET("", loginController.LoginInfo)
	login.POST("", loginController.Authenticate)

	// - logout
	e.POST("/logout", loginController.Logout, jwtValidator)

	// ~ Routes linked to user management ~
	users := e.Group("users")
	// basic crud
	users.POST("", userAccountController.Create)
	users.GET("", userAccountController.List, jwtValidator)
	users.GET("/:id", userAccountController.Read, jwtValidator)
	users.PATCH("/:id", userAccountController.PartialUpdate, jwtValidator)
	users.DELETE("/:id", userAccountController.Delete, jwtValidator)
	// confirmation routes
	users.POST("/confirm", userAccountController.Confirm)
	users.POST("/confirm/ask", userAccountController.AskConfirm)
	// password management
	users.PUT("/password", userAccountController.PasswordUpdate, jwtValidator)
	users.POST("/password/otp/confirm", userAccountController.PasswordConfirmOTP)
	users.POST("/password/reset", userAccountController.AskPasswordReset)
	users.PUT("/password/reset", userAccountController.PasswordReset)
	// avatar management
	users.PUT("/:id/avatar", userAccountController.AddAvatar, jwtValidator)
	users.DELETE("/:id/avatar", userAccountController.DeleteAvatar, jwtValidator)
	// backup management
	users.PUT("/:id/backup", userAccountController.AddBackup, jwtValidator)
	users.GET("/:id/backup", userAccountController.RetrieveBackup, jwtValidator)
	// public key management
	users.POST("/:id/pubkey", userAccountController.AddPublicKey, jwtValidator)
	users.GET("/:id/pubkey", userAccountController.RetrievePublicKey)
	// public info
	users.GET("/:email/public", userAccountController.ReadByEmail)

	// ~ Routes linked to user role  ~
	userRoles := e.Group("user-roles")
	userRoles.POST("", userRoleController.Create, jwtValidator)
	userRoles.GET("", userRoleController.List, jwtValidator)

	// SSO Client
	ssoClient := e.Group("sso-clients")
	ssoClient.POST("", ssoClientController.Create, jwtValidator)
	ssoClient.GET("/:id", ssoClientController.Get, jwtValidator)
	ssoClient.PATCH("/:id", ssoClientController.PartialUpdate, jwtValidator)
	ssoClient.POST("/:id/secret", ssoClientController.GenerateSecret, jwtValidator)

	// Bind generic routes
	generic := e.Group("")
	generic.GET("/version", genericController.Version)

	// ~ Optional bindings - mostly for development environment ~
	// - bind static assets for views only if configuration has been set up
	viewLocation := viper.GetString("server.views")
	if len(viewLocation) > 0 {
		e.Static("/", viewLocation)
	}
	// - bind static assets for avatars only if configuration has been set up
	avatarLocation := viper.GetString("server.avatars")
	if len(avatarLocation) > 0 {
		e.Static("/avatars", avatarLocation)
	}

	// finally launch the echo server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("server.port"))))
}

func initDefaultConfig() {
	// always look for the configuration file in the /etc folder
	viper.AddConfigPath("/etc/")

	// set defaults value for configuration
	viper.SetDefault("mail.from", "noreply@misakey.io")
	viper.SetDefault("hydra.secure", true)
	viper.SetDefault("feedback.secure", true)
	viper.SetDefault("databox.secure", true)
	viper.SetDefault("application.secure", true)
	viper.SetDefault("consent.secure", true)
	viper.SetDefault("server.port", 5000)

	// handle missing mandatory fields
	mandatoryFields := []string{
		"mail.templates",
		"auth.login_url", "auth.auth_url", "auth.token_url",
		"hydra.admin_endpoint", "hydra.public_endpoint",
		"feedback.endpoint",
		"databox.endpoint",
		"consent.endpoint",
		"application.client_id", "application.endpoint",
		"consent.endpoint",
		"app.client_id", "app.client_secret", "app.redirect_code_url", "app.redirect_token_url",
		"misadmin.client_id", "misadmin.client_secret", "misadmin.redirect_code_url", "misadmin.redirect_token_url",
		"server.cors",
		"cleanup.time_before_deletion",
	}
	secretFields := []string{
		"app.client_secret",
		"misadmin.client_secret",
	}

	// handle envs
	switch env {
	case "production":
		viper.SetConfigName("auth-config")
		mandatoryFields = append(mandatoryFields, []string{"aws.s3_region", "aws.ses_region", "aws.bucket", "aws.avatars_domain"}...)
		if os.Getenv("AWS_ACCESS_KEY") == "" {
			log.Warn().Msg("AWS_ACCESS_KEY not set")
		}
		if os.Getenv("AWS_SECRET_KEY") == "" {
			log.Warn().Msg("AWS_SECRET_KEY not set")
		}
	case "development":
		viper.SetConfigName("auth-config.dev")
		mandatoryFields = append(mandatoryFields, []string{"server.avatars", "server.avatar_url"}...)
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)
	config.Print(secretFields)
}

type mailerRepo interface {
	Send(context.Context, *model.Email) error
}

// getMailer defines mailer according to dev env varibable
func getMailer() mailerRepo {
	// handle envs
	switch env {
	case "production":
		return repo.NewMailerAmazonSES(viper.GetString("aws.ses_region"))
	case "development":
		return repo.NewLogMailer()
	}
	log.Fatal().Msg("unknown ENV value (should be production|development)")
	return nil
}

type avatarRepo interface {
	UploadAvatar(ctx context.Context, au *model.AvatarUpload) (string, error)
	DeleteAvatar(ad *model.AvatarDelete) error
}

func getAvatarRepo() avatarRepo {
	// handle envs
	switch env {
	case "production":
		avatarRepo, err := repo.NewAvatarAmazonS3(viper.GetString("aws.s3_region"), viper.GetString("aws.bucket"), viper.GetString("aws.avatars_domain"))
		if err != nil {
			log.Fatal().Err(err).Msg("cannot initiate AWS S3 avatar bucket connection")
		}
		return avatarRepo
	case "development":
		return repo.NewFileSystemAvatar(viper.GetString("server.avatars"), viper.GetString("server.avatar_url"))
	}
	log.Fatal().Msg("unknown ENV value (should be production|development)")
	return nil
}
