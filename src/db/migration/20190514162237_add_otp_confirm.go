package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190514162237, Down20190514162237)
}

func Up20190514162237(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN confirm_otp VARCHAR(255),
		ADD COLUMN confirm_otp_created_at timestamptz;
`)
	return err
}

func Down20190514162237(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN confirm_otp,
		DROP COLUMN confirm_otp_created_at;
	`)
	return err
}
