package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190513141111, Down20190513141111)
}

func Up20190513141111(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN reset_otp VARCHAR(255),
		ADD COLUMN reset_otp_created_at timestamptz ;
	`)
	return err
}

func Down20190513141111(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN reset_otp,
		DROP COLUMN reset_otp_created_at;
	`)
	return err
}
