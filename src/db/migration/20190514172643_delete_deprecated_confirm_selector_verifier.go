package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190514172643, Down20190514172643)
}

func Up20190514172643(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN confirm_selector,
		DROP COLUMN confirm_verifier;
	`)
	return err
}

func Down20190514172643(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN confirm_selector VARCHAR (255),
		ADD COLUMN confirm_verifier VARCHAR (255);
`)
	return err
}
