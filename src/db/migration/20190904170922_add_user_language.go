package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(up20190904170922, down20190904170922)
}

func up20190904170922(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TYPE LANGUAGE_ENUM AS ENUM ('fr', 'en')`)
	if err != nil {
		return fmt.Errorf("LANGUAGE_ENUM enum creation: %v", err)
	}

	_, err = tx.Exec(`
		ALTER TABLE user_account
		ADD COLUMN default_language LANGUAGE_ENUM
	;`)
	if err != nil {
		return fmt.Errorf("user_account table: %v", err)
	}

	return nil
}

func down20190904170922(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN default_language;
	`)
	return err
}
