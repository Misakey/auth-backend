package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190916103354, Down20190916103354)
}

func Up20190916103354(tx *sql.Tx) error {
	_, err := tx.Exec(`
		UPDATE role 
		SET
			label = 'admin'
		WHERE id = 1;
		`)
	if err != nil {
		return fmt.Errorf("failed to update role table value: %v", err)
	}

	_, err = tx.Exec(`
		UPDATE role 
		SET
			label = 'dpo'
		WHERE id = 2;
	`)
	if err != nil {
		return fmt.Errorf("failed to update role table value: %v", err)
	}

	_, err = tx.Exec(`
			ALTER TABLE role
			ADD CONSTRAINT role_id_label_key UNIQUE (id, label);
	`)
	if err != nil {
		return fmt.Errorf("failed to add constraint to table: %v", err)
	}
	return err
}

func Down20190916103354(tx *sql.Tx) error {
	_, err := tx.Exec(`
		UPDATE role 
		SET
			label = 'admin'
		WHERE id = 2;
	`)
	if err != nil {
		return fmt.Errorf("failed to update role table value: %v", err)
	}

	_, err = tx.Exec(`
		UPDATE role 
		SET
			label = 'dpo'
		WHERE id = 1;
	`)
	if err != nil {
		return fmt.Errorf("failed to update role table value: %v", err)
	}

	_, err = tx.Exec(`
		ALTER TABLE role
		DROP CONSTRAINT role_id_label_key;
	`)
	if err != nil {
		return fmt.Errorf("failed to drop constraint to table: %v", err)
	}
	return err
}
