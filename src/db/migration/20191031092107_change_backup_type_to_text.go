package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upChangeBackupTypeToText, downChangeBackupTypeToText)
}

func upChangeBackupTypeToText(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE backup ALTER COLUMN data SET DATA TYPE TEXT`)
	if err != nil {
		return fmt.Errorf("changing column data of table backup to type text: %v", err)
	}

	return err
}

// XXX Rolling back will probably break things
// as values will be too large to fit in VARCHAR(255)
// (this is the reason we are switching to TEXT)
func downChangeBackupTypeToText(tx *sql.Tx) error {
	_, err := tx.Exec(`UPDATE backup SET data='' WHERE char_length(data)>254`)
	if err != nil {
		return fmt.Errorf("emptying backup data if longer than 255 chars: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE backup ALTER COLUMN data SET DATA TYPE VARCHAR(255)`)
	if err != nil {
		return fmt.Errorf("changing column data of table backup to type varchar(255): %v", err)
	}

	return err
}
