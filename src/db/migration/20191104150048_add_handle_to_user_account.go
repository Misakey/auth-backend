package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20191001151817, Down20191001151817)
}

func Up20191001151817(tx *sql.Tx) error {
	_, err := tx.Exec(`
		ALTER TABLE user_account
		ADD COLUMN handle VARCHAR(32) UNIQUE NOT NULL;
	`)
	if err != nil {
		return fmt.Errorf("adding handle column: %s", err.Error())
	}
	return nil
}

func Down20191001151817(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account DROP COLUMN handle;`)
	if err != nil {
		return fmt.Errorf("dropping handle column: %s", err.Error())
	}
	return nil
}
