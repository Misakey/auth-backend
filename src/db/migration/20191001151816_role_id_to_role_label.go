package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20191001151816, Down20191001151816)
}

func Up20191001151816(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TYPE ROLE_LABEL_ENUM AS ENUM(
		'admin',
		'dpo'
	);`)
	if err != nil {
		return fmt.Errorf("creating user role label enum: %v", err)
	}

	// Drop constraint associated with role_id
	_, err = tx.Exec(`ALTER TABLE user_role
		DROP CONSTRAINT user_role_user_id_role_id_application_id_key;
		`)
	if err != nil {
		return fmt.Errorf("failed to drop constraint: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		ADD label_tmp ROLE_LABEL_ENUM UNIQUE;
	`)
	if err != nil {
		return fmt.Errorf("failed to add constraint role_label: %v", err)
	}

	_, err = tx.Exec(`UPDATE role
		SET label_tmp = 'admin'
		WHERE label = 'admin'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'admins': %v", err)
	}

	_, err = tx.Exec(`UPDATE role
		SET label_tmp = 'dpo'
		WHERE label = 'dpo'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'dpo': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		ADD CONSTRAINT role_label_key UNIQUE (label)
	;`)
	if err != nil {
		return fmt.Errorf("adding 'unique' constraint on role label: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		ALTER COLUMN label_tmp SET NOT NULL
	;`)
	if err != nil {
		return fmt.Errorf("adding 'not null' constraint on role label: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		DROP label
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label' column: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		RENAME label_tmp TO label
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label': %v", err)
	}

	// Add new column role_label
	_, err = tx.Exec(`ALTER TABLE user_role
		ADD COLUMN role_label ROLE_LABEL_ENUM NOT NULL DEFAULT 'dpo' REFERENCES role(label) ON DELETE CASCADE
	;`)
	if err != nil {
		return fmt.Errorf("failed to add column role_label: %v", err)
	}

	// Insert role_label depending on values of role_id
	_, err = tx.Exec(`UPDATE user_role
		SET role_label = 'admin'
		WHERE role_id = 1
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'admin': %v", err)
	}
	_, err = tx.Exec(`UPDATE user_role
		SET role_label = 'dpo'
		WHERE role_id = 2
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'dpo': %v", err)
	}

	// Make role_label - user_id unique
	_, err = tx.Exec(`ALTER TABLE user_role
		ADD CONSTRAINT user_role_user_id_role_label_application_id_key UNIQUE (user_id, role_label, application_id)
	;`)
	if err != nil {
		return fmt.Errorf("failed to add uniqueness of role_label - user_id: %v", err)
	}

	// Delete unused column
	_, err = tx.Exec(`ALTER TABLE user_role
		DROP COLUMN role_id
	;`)
	return err
}

func Down20191001151816(tx *sql.Tx) error {

	// Add constraint associated with role_id
	_, err := tx.Exec(`ALTER TABLE user_role
		DROP CONSTRAINT user_role_user_id_role_label_application_id_key,
		DROP CONSTRAINT user_role_role_label_fkey;
   `)
	if err != nil {
		return fmt.Errorf("failed to add constraint: %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		RENAME label TO label_tmp;
	;`)
	if err != nil {
		return fmt.Errorf("renaming column to 'label_tmp': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		ADD label VARCHAR(255)
	;`)
	if err != nil {
		return fmt.Errorf("adding column label: %v", err)
	}

	_, err = tx.Exec(`UPDATE role
		SET label = 'admin'
		WHERE label_tmp = 'admin'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'admin': %v", err)
	}
	_, err = tx.Exec(`UPDATE role
		SET label = 'dpo'
		WHERE label_tmp = 'dpo'
	;`)
	if err != nil {
		return fmt.Errorf("setting label 'dpo': %v", err)
	}

	_, err = tx.Exec(`ALTER TABLE role
		DROP label_tmp
	;`)
	if err != nil {
		return fmt.Errorf("dropping 'label_tmp' column: %v", err)
	}

	// Add new unique contraint
	_, err = tx.Exec(`ALTER TABLE role
		ADD CONSTRAINT role_id_label_key UNIQUE (id, label)
	;`)
	if err != nil {
		return fmt.Errorf("failed to drop or add constraint: %v", err)
	}

	// Add column role_id
	_, err = tx.Exec(`ALTER TABLE user_role
		ADD COLUMN role_id INTEGER NOT NULL DEFAULT 2 REFERENCES role ON DELETE CASCADE,
		ADD CONSTRAINT user_role_user_id_role_id_application_id_key UNIQUE (user_id, role_id, application_id)
	;`)
	if err != nil {
		return fmt.Errorf("failed to add column role_id: %v", err)
	}

	// Insert role_id depending on values of role_label
	_, err = tx.Exec(`UPDATE user_role
		SET role_id = 1
		WHERE role_label = 'admin'
	;`)
	if err != nil {
		return fmt.Errorf("setting role_id to 1: %v", err)
	}

	_, err = tx.Exec(`UPDATE user_role
		SET role_id = 2
		WHERE role_label = 'dpo'
	;`)
	if err != nil {
		return fmt.Errorf("setting role_id to 2: %v", err)
	}

	// Delete unused column
	_, err = tx.Exec(`ALTER TABLE user_role
		DROP COLUMN role_label;
	`)
	return err
}
