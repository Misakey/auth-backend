package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190603091607, Down20190603091607)
}

func Up20190603091607(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE custom_access(
		token VARCHAR(255) PRIMARY KEY,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		subject VARCHAR(255) NOT NULL,
		scope VARCHAR(255) NOT NULL
		);`)
	return err
}

func Down20190603091607(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE custom_access;`)
	return err
}
