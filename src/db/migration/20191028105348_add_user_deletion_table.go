package migration

import (
	"database/sql"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upAddUserDeletionTable, downAddUserDeletionTable)
}

func upAddUserDeletionTable(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE user_deletion(
 		id SERIAL PRIMARY KEY,
		user_id UUID UNIQUE NOT NULL REFERENCES user_account ON DELETE CASCADE,
		notified BOOLEAN NOT NULL DEFAULT false,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
	);`)
	return err
}

func downAddUserDeletionTable(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE user_deletion;`)
	return err
}
