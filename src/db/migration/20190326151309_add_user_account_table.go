package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190326151309, Down20190326151309)
}

func Up20190326151309(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE user_account(
 		id UUID PRIMARY KEY,
 		email VARCHAR (255) UNIQUE NOT NULL,
 		password VARCHAR (255) NOT NULL,
		confirmed BOOLEAN NOT NULL DEFAULT false,
		confirm_selector VARCHAR (255) NOT NULL,
		confirm_verifier VARCHAR (255) NOT NULL,
		keep_alive_duration INT NOT NULL
	);`)
	return err
}

func Down20190326151309(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE user_account;`)
	return err
}
