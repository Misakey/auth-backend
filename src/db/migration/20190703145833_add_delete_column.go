package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190703145833, Down20190703145833)
}

func Up20190703145833(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN to_delete BOOLEAN NOT NULL DEFAULT false,
		ADD COLUMN created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		ADD COLUMN updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP;
		ALTER TABLE public_key
		ADD COLUMN to_delete BOOLEAN NOT NULL DEFAULT false;
		ALTER TABLE backup
		ADD COLUMN to_delete BOOLEAN NOT NULL DEFAULT false;
`)
	return err
}

func Down20190703145833(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN to_delete,
		DROP COLUMN created_at,
		DROP COLUMN updated_at;
		ALTER TABLE public_key DROP COLUMN to_delete;
		ALTER TABLE backup DROP COLUMN to_delete;
	`)
	return err
}
