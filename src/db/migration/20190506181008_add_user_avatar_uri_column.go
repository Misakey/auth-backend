package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190506181008, Down20190506181008)
}

func Up20190506181008(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN avatar_uri VARCHAR(255);
	`)
	return err
}

func Down20190506181008(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN avatar_uri;
	`)
	return err
}
