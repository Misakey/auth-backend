package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190405144800, Down20190405144800)
}

func Up20190405144800(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ADD COLUMN display_name VARCHAR(255);
	`)
	return err
}

func Down20190405144800(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		DROP COLUMN display_name;
	`)
	return err
}
