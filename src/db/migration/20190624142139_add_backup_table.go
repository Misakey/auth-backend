package migration

import (
	"database/sql"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190624142139, Down20190624142139)
}

func Up20190624142139(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE backup(
		id SERIAL PRIMARY KEY,
		user_id UUID NOT NULL REFERENCES user_account ON DELETE CASCADE UNIQUE,
		data VARCHAR(255) NOT NULL,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
   );`)
   return err
}

func Down20190624142139(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE backup;`)
	return err
}
