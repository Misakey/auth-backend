package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190502150841, Down20190502150841)
}

func Up20190502150841(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ALTER COLUMN display_name SET NOT NULL;
	`)
	return err
}

func Down20190502150841(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE user_account
		ALTER COLUMN display_name DROP NOT NULL;
	`)
	return err
}
