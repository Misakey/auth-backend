package migration

import (
	"database/sql"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190625143927, Down20190625143927)
}

func Up20190625143927(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE public_key(
		id SERIAL PRIMARY KEY,
		user_id UUID NOT NULL REFERENCES user_account ON DELETE CASCADE,
		pubkey VARCHAR(255) NOT NULL,
		created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP
   );`)
   return err

}

func Down20190625143927(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE public_key;`)
	return err
}
