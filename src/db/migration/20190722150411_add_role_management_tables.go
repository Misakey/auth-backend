package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190722150411, Down20190722150411)
}

func Up20190722150411(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE role(
		id SERIAL PRIMARY KEY,
		label VARCHAR(255) NOT NULL
   );`)
	if err != nil {
		return fmt.Errorf("role table: %v", err)
	}

	_, err = tx.Exec(`
		INSERT INTO role (id, label)
		VALUES
			(1, 'dpo'),
   			(2, 'admin')
   `)
	if err != nil {
		return fmt.Errorf("failed to insert role into table: %v", err)
	}

	_, err = tx.Exec(`CREATE TABLE user_role(
		id SERIAL PRIMARY KEY,
		user_id UUID NOT NULL REFERENCES user_account ON DELETE CASCADE,
		role_id INTEGER NOT NULL REFERENCES role ON DELETE CASCADE,
		application_id UUID NOT NULL,
		UNIQUE (user_id, role_id, application_id)
   );`)
	return err
}

func Down20190722150411(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE user_role;`)
	if err != nil {
		return fmt.Errorf("failed to drop user_role table: %v", err)
	}

	_, err = tx.Exec(`DROP TABLE role;`)
	if err != nil {
		return fmt.Errorf("failed to drop role table: %v", err)
	}
	return err
}
