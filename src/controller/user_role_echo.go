package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserRoleEcho provides Echo Handler functions interacting with a user roles
type UserRoleEcho struct {
	service serviceUserRole
}

// NewUserRoleEcho is UserRoleEcho constructor
func NewUserRoleEcho(service serviceUserRole) *UserRoleEcho {
	return &UserRoleEcho{
		service: service,
	}
}

// Handles user role creation request
func (c *UserRoleEcho) Create(ctx echo.Context) error {
	userRole := model.UserRole{}

	err := ctx.Bind(&userRole)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	err = ctx.Validate(&userRole)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.CreateRole(ctx.Request().Context(), &userRole)
	if err != nil {
		return merror.
			Transform(err).
			Describef("could not create").
			From(merror.OriBody)
	}
	return ctx.JSON(http.StatusCreated, userRole)
}

func (c *UserRoleEcho) List(ctx echo.Context) error {
	filters := model.UserRoleFilters{}
	filters.UserID = ctx.QueryParam("user_id")

	err := ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	userRoles, err := c.service.ListRoles(ctx.Request().Context(), filters)
	if err != nil {
		return merror.
			Transform(err).
			Describef("could not list").
			From(merror.OriQuery)
	}

	return ctx.JSON(http.StatusOK, userRoles)
}
