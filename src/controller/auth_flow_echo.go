package controller

import (
	"strings"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/auth"
)

// AuthFlowEcho
type AuthFlowEcho struct {
	mauth         *auth.AuthorizationCodeFlow
	defaultScopes string
}

// NewAuthFlowEcho is AuthFlowEcho constructor
// configured defaultScopes are added to requested scopes for all requests
// defaultScopes must be a space-separated list of scopes
func NewAuthFlowEcho(
	mauth *auth.AuthorizationCodeFlow,
	defaultScopes string) *AuthFlowEcho {
	return &AuthFlowEcho{mauth: mauth, defaultScopes: defaultScopes}
}

// RequestCode starts an authorization code flow
func (c *AuthFlowEcho) RequestCode(ctx echo.Context) error {
	c.addDefaultScopes(ctx)
	c.mauth.RequestCode(ctx.Response().Writer, ctx.Request())
	return nil
}

// ExchangeToken starts exchanging code against a token
func (c *AuthFlowEcho) ExchangeToken(ctx echo.Context) error {
	c.addDefaultScopes(ctx)
	c.mauth.ExchangeToken(ctx.Response().Writer, ctx.Request())
	return nil
}

// addDefaultScopes concats optionally configured default scopes to existing one in current request
func (c *AuthFlowEcho) addDefaultScopes(ctx echo.Context) {
	if c.defaultScopes == "" {
		return
	}

	q := ctx.Request().URL.Query()
	var scopes strings.Builder
	// add existing scopes
	scopes.WriteString(q.Get("scope"))
	if scopes.Len() > 0 {
		scopes.WriteString(" ")
	}
	scopes.WriteString(c.defaultScopes)
	q.Set("scope", scopes.String())
	ctx.Request().URL.RawQuery = q.Encode()
}
