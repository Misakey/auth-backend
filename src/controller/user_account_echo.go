package controller

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/auth-backend/src/adaptor/slices"
	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserAccountEcho provides Echo Handler functions interacting with a user account service
type UserAccountEcho struct {
	service serviceUserAccount
}

// NewUserAccountEcho is UserAccountEcho constructor
func NewUserAccountEcho(service serviceUserAccount) *UserAccountEcho {
	return &UserAccountEcho{
		service: service,
	}
}

// Handles user account creation request
func (c *UserAccountEcho) Create(ctx echo.Context) error {
	user := model.UserAccount{}

	err := ctx.Bind(&user)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&user)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.Create(ctx.Request().Context(), &user)
	if err != nil {
		return merror.Transform(err).Describef("could not create").From(merror.OriBody)
	}
	return ctx.JSON(http.StatusCreated, user)
}

// Read is the handler for user account GET request
func (c *UserAccountEcho) Read(ctx echo.Context) error {
	// validate id is an uuid
	id := ctx.Param("id")
	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Describe(err.Error()).Detail("id", merror.DVMalformed)
	}

	ua, err := c.service.Read(ctx.Request().Context(), id)
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, ua)
}

// ReadByEmail is the handler for user account email GET Request
func (c *UserAccountEcho) ReadByEmail(ctx echo.Context) error {
	email := ctx.Param("email")

	ua, err := c.service.ReadByEmail(ctx.Request().Context(), email)
	if err != nil {
		return merror.Transform(err).From(merror.OriPath)
	}
	return ctx.JSON(http.StatusOK, ua)
}

// List is the handler for users GET Request
func (c *UserAccountEcho) List(ctx echo.Context) error {
	filters := model.UserAccountFilters{}
	filters.IDs = slices.FromSep(ctx.QueryParam("ids"), ",")

	err := ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	users, err := c.service.List(ctx.Request().Context(), filters)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, users)
}

// PartialUpdate is the handler for user account PATCH request
func (c *UserAccountEcho) PartialUpdate(ctx echo.Context) error {
	// bind patched data to our model
	patchMap := patch.FromHTTP(ctx.Request()).ToModel(model.UserAccount{})
	if patchMap.Invalid {
		return merror.BadRequest().From(merror.OriBody).Describe("invalid patch entity")
	}

	ua := model.UserAccount{}
	patchInfo := patchMap.GetInfo(&ua)
	err := ctx.Bind(&ua)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	// validate the patch of the entity
	err = ctx.Validate(&patchInfo)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	// validate id is an UUID
	id := ctx.Param("id")
	_, err = uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}
	ua.ID = id

	err = c.service.PartialUpdate(ctx.Request().Context(), &patchInfo)
	if err != nil {
		return merror.Transform(err).Describef("could not patch")
	}
	return ctx.NoContent(http.StatusNoContent)
}

// PasswrodUpdate is the handler for user account PUT password
func (c *UserAccountEcho) PasswordUpdate(ctx echo.Context) error {
	pu := model.PasswordUpdate{}

	err := ctx.Bind(&pu)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&pu)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.PasswordUpdate(ctx.Request().Context(), &pu)
	if err != nil {
		return merror.Transform(err).Describef("could not update password")
	}

	return ctx.NoContent(http.StatusNoContent)
}

// PasswordConfirmOTP is the handler for user account POST password otp confirm
func (c *UserAccountEcho) PasswordConfirmOTP(ctx echo.Context) error {
	poc := &model.PasswordConfirmOTP{}

	err := ctx.Bind(poc)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(poc)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	poc, err = c.service.PasswordConfirmOTP(ctx.Request().Context(), poc)
	if err != nil {
		return merror.Transform(err).Describef("could not validate password otp")
	}

	return ctx.JSON(http.StatusOK, poc)
}

// AskPasswordReset is the handler for user account POST password reset
func (c *UserAccountEcho) AskPasswordReset(ctx echo.Context) error {
	apr := model.AskPasswordReset{}

	err := ctx.Bind(&apr)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&apr)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.AskPasswordReset(ctx.Request().Context(), &apr)
	if err != nil {
		return merror.Transform(err).Describef("could not ask reset password")
	}

	return ctx.NoContent(http.StatusNoContent)
}

// PasswordReset is the handler for user account PUT password reset
func (c *UserAccountEcho) PasswordReset(ctx echo.Context) error {
	pr := model.PasswordReset{}

	err := ctx.Bind(&pr)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&pr)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.PasswordReset(ctx.Request().Context(), &pr)
	if err != nil {
		return merror.Transform(err).Describef("could not reset password")
	}

	return ctx.NoContent(http.StatusNoContent)
}

// Confirm is the handler for user account confirm OTP request
func (c *UserAccountEcho) Confirm(ctx echo.Context) error {
	confirmOTP := model.UserAccountConfirmOTP{}

	err := ctx.Bind(&confirmOTP)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&confirmOTP)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.Confirm(ctx.Request().Context(), &confirmOTP)
	if err != nil {
		return err
	}
	return ctx.NoContent(http.StatusNoContent)
}

// AskConfirm is the handler for user account confirm reset OTP request
func (c *UserAccountEcho) AskConfirm(ctx echo.Context) error {
	askOTP := model.UserAccountAskConfirm{}

	err := ctx.Bind(&askOTP)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&askOTP)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = c.service.AskConfirm(ctx.Request().Context(), &askOTP)
	if err != nil {
		return err
	}
	return ctx.NoContent(http.StatusNoContent)
}

// AddAvatar : handler for user user avatar creation/update
func (c *UserAccountEcho) AddAvatar(ctx echo.Context) error {
	au := model.AvatarUpload{}

	id := ctx.Param("id")
	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	file, err := ctx.FormFile("avatar")
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Detail("avatar", merror.DVRequired).Describe(err.Error())
	}
	if file.Size >= 3*1024*1024 {
		return merror.BadRequest().From(merror.OriBody).Detail("size", merror.DVInvalid).Describe("size must be < 3 mo")
	}

	f, err := file.Open()
	if err != nil {
		return err
	}

	au.Size = file.Size
	au.UserID = id
	au.Data = f

	err = c.service.AddAvatar(ctx.Request().Context(), &au)
	if err != nil {
		return merror.Transform(err).Describef("could not upload the avatar")
	}

	return ctx.NoContent(http.StatusNoContent)
}

// DeleteAvatar : handler for user user avatar deletion
func (c *UserAccountEcho) DeleteAvatar(ctx echo.Context) error {
	ad := model.AvatarDelete{}

	id := ctx.Param("id")
	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	ad.UserID = id

	err = c.service.DeleteAvatar(ctx.Request().Context(), &ad)
	if err != nil {
		return merror.Transform(err).Describef("could not delete the avatar")
	}

	return ctx.NoContent(http.StatusNoContent)
}

// AddBackup : handler for user backup creation
func (c *UserAccountEcho) AddBackup(ctx echo.Context) error {
	backup := model.Backup{}

	userID := ctx.Param("id")
	_, err := uuid.Parse(userID)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	err = ctx.Bind(&backup)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&backup)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	backup.UserID = userID

	err = c.service.AddBackup(ctx.Request().Context(), &backup)
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusCreated, backup)
}

// AddPublicKey : handler for user public key creation
func (c *UserAccountEcho) AddPublicKey(ctx echo.Context) error {
	pk := model.PublicKey{}

	userID := ctx.Param("id")
	_, err := uuid.Parse(userID)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	err = ctx.Bind(&pk)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&pk)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	pk.UserID = userID

	err = c.service.AddPublicKey(ctx.Request().Context(), &pk)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusCreated, pk)
}

// RetrieveBackup : handler for user backup retrieval
func (c *UserAccountEcho) RetrieveBackup(ctx echo.Context) error {
	userID := ctx.Param("id")
	_, err := uuid.Parse(userID)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	backup, err := c.service.RetrieveBackup(ctx.Request().Context(), userID)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, backup)
}

// RetrievePublicKey : handler for user public key retrieval
func (c *UserAccountEcho) RetrievePublicKey(ctx echo.Context) error {
	userID := ctx.Param("id")
	_, err := uuid.Parse(userID)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	pk, err := c.service.RetrievePublicKey(ctx.Request().Context(), userID)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, pk)
}

// Partially delete a user
func (c *UserAccountEcho) Delete(ctx echo.Context) error {
	userID := ctx.Param("id")
	_, err := uuid.Parse(userID)
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVMalformed).Describe(err.Error())
	}

	err = c.service.Delete(ctx.Request().Context(), userID)
	if err != nil {
		return err
	}

	return ctx.NoContent(http.StatusNoContent)
}
