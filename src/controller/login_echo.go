package controller

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// LoginEcho provides function to bind to routes interacting with login
type LoginEcho struct {
	service serviceLogin
}

// NewLoginEcho is LoginEcho constructor
func NewLoginEcho(service serviceLogin) *LoginEcho {
	return &LoginEcho{
		service: service,
	}
}

// Handles Hydra login flow request
func (c *LoginEcho) LoginInfo(ctx echo.Context) error {
	// parse parameters
	challenge := ctx.QueryParam("login_challenge")
	// challenge login then redirect
	redirectURL := c.service.LoginInfo(ctx.Request().Context(), challenge)
	return ctx.Redirect(http.StatusFound, redirectURL)
}

// Handles user authentication request
func (c *LoginEcho) Authenticate(ctx echo.Context) error {
	// check body parameters
	authReq := model.AuthRequest{}
	err := ctx.Bind(&authReq)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}
	err = ctx.Validate(&authReq)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	// challenge login then redirect
	acceptInfo, err := c.service.Authenticate(ctx.Request().Context(), authReq)
	if err != nil {
		return merror.Transform(err).Describef("could not authenticate")
	}
	// do not redirect - frontend browser does not get this redirection
	return ctx.JSON(http.StatusSeeOther, acceptInfo)
}

// Logout : handler for user logout
func (c *LoginEcho) Logout(ctx echo.Context) error {
	logoutReq := model.LogoutRequest{}
	err := ctx.Bind(&logoutReq)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&logoutReq)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	_, err = uuid.Parse(logoutReq.UserID)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Detail("user_id", merror.DVMalformed).Describe(err.Error())
	}

	err = c.service.Logout(ctx.Request().Context(), logoutReq.UserID)
	if err != nil {
		return merror.Transform(err).Describef("could not logout the user")
	}
	return ctx.NoContent(http.StatusNoContent)
}
