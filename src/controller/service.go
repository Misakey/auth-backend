package controller

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/auth-backend/src/model"
)

type serviceSSOClient interface {
	Create(ctx context.Context, hydraClient *model.HydraClient) error
	Get(ctx context.Context, id string) (*model.HydraClient, error)
	PartialUpdate(ctx context.Context, pi *patch.Info) error
	GenerateSecret(ctx context.Context, id string) (string, error)
}

type serviceLogin interface {
	LoginInfo(ctx context.Context, challenge string) string
	Authenticate(ctx context.Context, authReq model.AuthRequest) (*model.LoginAcceptInfo, error)
	Logout(ctx context.Context, userID string) error
}

type serviceToken interface {
	GetAccessJWT(ctx context.Context, opaqueTok string) (string, error)
	CreateCustomAccess(ctx context.Context, ca *model.CustomAccess) error
	GetAccessJWTCustom(ctx context.Context, opaqueTok string) (string, error)
}

type serviceUserAccount interface {
	Create(ctx context.Context, user *model.UserAccount) error
	Read(ctx context.Context, id string) (*model.UserAccount, error)
	ReadByEmail(ctx context.Context, email string) (*model.UserPublicInfo, error)
	List(ctx context.Context, filters model.UserAccountFilters) ([]*model.UserAccount, error)
	PartialUpdate(ctx context.Context, uaPatch *patch.Info) error
	PasswordUpdate(ctx context.Context, pwdUpdate *model.PasswordUpdate) error
	PasswordReset(ctx context.Context, pwdUpdate *model.PasswordReset) error
	PasswordConfirmOTP(ctx context.Context, poc *model.PasswordConfirmOTP) (*model.PasswordConfirmOTP, error)
	AskPasswordReset(ctx context.Context, pwdAskRst *model.AskPasswordReset) error
	Confirm(ctx context.Context, confirmOTP *model.UserAccountConfirmOTP) error
	AskConfirm(ctx context.Context, askOTP *model.UserAccountAskConfirm) error
	Delete(ctx context.Context, id string) error

	AddAvatar(ctx context.Context, au *model.AvatarUpload) error
	DeleteAvatar(ctx context.Context, ad *model.AvatarDelete) error

	AddBackup(ctx context.Context, backup *model.Backup) error
	RetrieveBackup(ctx context.Context, userID string) (*model.Backup, error)
	AddPublicKey(ctx context.Context, pk *model.PublicKey) error
	RetrievePublicKey(ctx context.Context, userID string) (*model.PublicKey, error)
}

type serviceUserRole interface {
	CreateRole(ctx context.Context, user *model.UserRole) error
	ListRoles(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error)
}
