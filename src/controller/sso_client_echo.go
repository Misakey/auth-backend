package controller

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/auth-backend/src/model"
)

type clientSecret struct {
	Secret string `json:"client_secret"`
}

// SSOClientEcho provides functions which bind routes and SSOClient
type SSOClientEcho struct {
	service serviceSSOClient
}

// NewSSOClientEcho is SSOClientEcho constructor
func NewSSOClientEcho(service serviceSSOClient) *SSOClientEcho {
	return &SSOClientEcho{
		service: service,
	}
}

// Create is the handler for sso-clients POST request
func (sce *SSOClientEcho) Create(ctx echo.Context) error {
	hydraClient := model.HydraClient{}

	err := ctx.Bind(&hydraClient)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error())
	}

	err = ctx.Validate(&hydraClient)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = sce.service.Create(ctx.Request().Context(), &hydraClient)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusCreated, hydraClient)
}

// PartialUpdate is the handler for sso-clients/:id PATCH request
func (sce *SSOClientEcho) PartialUpdate(ctx echo.Context) error {
	id := ctx.Param("id")
	_, err := uuid.Parse(id)
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Describe(err.Error()).Detail("id", merror.DVInvalid)
	}

	// bind patched data to our model
	patchMap := patch.FromHTTP(ctx.Request()).ToModel(model.HydraClientPatch{})
	if patchMap.Invalid {
		return merror.BadRequest().From(merror.OriBody).Describe("invalid patch map").Detail("body", merror.DVInvalid)
	}

	ssoClient := model.HydraClientPatch{}
	ssoClient.ID = id
	patchInfo := patchMap.GetInfo(&ssoClient)
	err = ctx.Bind(&ssoClient)
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Describe(err.Error()).Detail("body", merror.DVInvalid)
	}

	// Validate
	err = ctx.Validate(&patchInfo)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = sce.service.PartialUpdate(ctx.Request().Context(), &patchInfo)
	if err != nil {
		return merror.Transform(err).Describef("could not patch sso client")
	}
	return ctx.NoContent(http.StatusNoContent)

}

// GenerateSecret is the handler for sso-client/:id/secret POST request
func (c *SSOClientEcho) GenerateSecret(ctx echo.Context) error {
	id := ctx.Param("id")
	clientSecret := clientSecret{}

	secret, err := c.service.GenerateSecret(ctx.Request().Context(), id)
	if err != nil {
		return err
	}

	clientSecret.Secret = secret

	return ctx.JSON(http.StatusCreated, clientSecret)
}

// Get is the handler for sso-client/:id GET request
func (sce *SSOClientEcho) Get(ctx echo.Context) error {
	id := ctx.Param("id")

	ssoClient, err := sce.service.Get(ctx.Request().Context(), id)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, ssoClient)
}
