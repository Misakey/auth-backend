package repo

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// AvatarFileSystem
type AvatarFileSystem struct {
	location  string
	avatarURL string
}

// NewFileSystemAvatar init
func NewFileSystemAvatar(avatarLocation, avatarURL string) *AvatarFileSystem {
	// Create avatars directory
	if _, err := os.Stat(avatarLocation); os.IsNotExist(err) {
		os.Mkdir(avatarLocation, os.ModePerm)
	}
	return &AvatarFileSystem{
		location:  avatarLocation,
		avatarURL: avatarURL,
	}
}

// UploadAvatar : Write an avatar in directory and store the path in the db
func (al *AvatarFileSystem) UploadAvatar(ctx context.Context, au *model.AvatarUpload) (string, error) {
	body, err := ioutil.ReadAll(au.Data)
	if err != nil {
		return "", err
	}

	filePath := path.Join(al.location, "/", au.Filename)
	f, err := os.Create(filePath)
	if err != nil {
		return "", err
	}

	_, err = f.Write(body)
	if err != nil {
		f.Close()
		return "", err
	}

	err = f.Close()
	if err != nil {
		return "", err
	}

	return al.avatarURL + "/" + au.Filename, nil
}

// DeleteAvatar : Delete the avatar in file System
func (al *AvatarFileSystem) DeleteAvatar(ad *model.AvatarDelete) error {
	path := path.Join(al.location, "/", ad.Filename)
	err := os.Remove(path)
	if err != nil {
		return err
	}

	fmt.Println("File " + ad.Filename + " Deleted")
	return nil
}
