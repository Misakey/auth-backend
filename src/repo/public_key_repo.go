package repo

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// PublicKey manages user public key storage using PSQL database and sqlboiler ORM
type PublicKeyPSQL struct {
	db *sql.DB
}

// NewPublicKey is PublicKey constructor
func NewPublicKey(db *sql.DB) *PublicKeyPSQL {
	return &PublicKeyPSQL{
		db: db,
	}
}

// Insert a public key
func (p *PublicKeyPSQL) Insert(ctx context.Context, pk *model.PublicKey) error {
	return pk.Insert(ctx, p.db, boil.Infer())
}

// GetByUserID will look up the most recent public key based on the passed userID
func (p *PublicKeyPSQL) GetByUserID(ctx context.Context, userID string) (*model.PublicKey, error) {
	pk, err := model.PublicKeys(qm.Where("user_id=?", userID), qm.Where("to_delete=?", false), qm.OrderBy("created_at")).One(ctx, p.db)

	// err no row means user was not found
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("user_id", merror.DVNotFound)
	}
	return pk, err
}

// Update persists the public key in the database
func (p *PublicKeyPSQL) Update(ctx context.Context, pk *model.PublicKey) error {
	rowsAff, err := pk.Update(ctx, p.db, boil.Infer())
	if err != nil {
		return err
	}

	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		return merror.NotFound().Describe("no row affected").Detail("id", merror.DVNotFound)
	}

	return nil
}
