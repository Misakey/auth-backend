package repo

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserDeletionSQLBoiler manages users deletions storage using PSQL database and sqlboiler ORM
type UserDeletionSQLBoiler struct {
	db *sql.DB
}

// NewUserDeletionSQLBoiler is UserDeletionSQLBoiler constructor
func NewUserDeletionSQLBoiler(db *sql.DB) *UserDeletionSQLBoiler {
	return &UserDeletionSQLBoiler{
		db: db,
	}
}

// Insert the user deletion object
func (uds *UserDeletionSQLBoiler) Insert(ctx context.Context, userDeletion *model.UserDeletion) error {
	return userDeletion.Insert(ctx, uds.db, boil.Infer())
}

// PartialUpdate
func (uds *UserDeletionSQLBoiler) Update(ctx context.Context, userDeletion *model.UserDeletion) error {
	rowsAff, err := userDeletion.Update(ctx, uds.db, boil.Infer())
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected").Detail("id", merror.DVNotFound)
	}
	return nil
}

// List in progress users deletions depending on some filters
func (uds *UserDeletionSQLBoiler) List(ctx context.Context, filters model.UserDeletionFilters) ([]*model.UserDeletion, error) {
	mods := []qm.QueryMod{}
	mods = append(mods, qm.Select(`user_deletion.*`, `user_account.email`))
	if !filters.Notified.IsZero() {
		mods = append(mods, qm.Where(`user_deletion.notified = ?`, filters.Notified))
	}
	if !filters.UpdatedBefore.IsZero() {
		mods = append(mods, qm.Where(`user_deletion.updated_at < ?`, filters.UpdatedBefore))
	}
	if !filters.CreatedBefore.IsZero() {
		mods = append(mods, qm.Where(`user_deletion.created_at < ?`, filters.CreatedBefore))
	}

	var users []*model.UserDeletion
	mods = append(mods, qm.InnerJoin(`user_account on user_deletion.user_id=user_account.id`))
	err := model.UserDeletions(mods...).Bind(ctx, uds.db, &users)
	if err == nil && users == nil {
		return []*model.UserDeletion{}, nil
	}
	return users, err
}
