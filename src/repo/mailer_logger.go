package repo

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// MailerLogger logs emails instead of sending them.
type MailerLogger struct {
}

// NewMailerLogger creates a mailer that doesn't deliver emails but simply logs them.
func NewLogMailer() *MailerLogger {
	return &MailerLogger{}
}

// Send an email (log only text)
func (l MailerLogger) Send(ctx context.Context, email *model.Email) error {
	logger.
		FromCtx(ctx).
		Info().
		Msgf("===> EMAIL SENT TO %s FROM %s: [%s]", email.To, email.From, email.Subject)
	logger.
		FromCtx(ctx).
		Info().
		Msg(email.TextBody)
	return nil
}
