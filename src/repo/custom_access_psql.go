package repo

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// CustomAccessPSQL manages custom access storage using sqlboiler
type CustomAccessPSQL struct {
	db *sql.DB
}

// NewCustomAccessPSQL is CustomAccessPSQL constructor
func NewCustomAccessPSQL(db *sql.DB) *CustomAccessPSQL {
	return &CustomAccessPSQL{
		db: db,
	}
}

// Insert the custom access in storage, it should not overwrite a custom access
func (p *CustomAccessPSQL) Insert(ctx context.Context, ca *model.CustomAccess) error {
	return ca.Insert(ctx, p.db, boil.Infer())
}

// Retrieve a CustomAccess resource using unique token column
func (p *CustomAccessPSQL) Get(ctx context.Context, token string) (*model.CustomAccess, error) {
	user, err := model.CustomAccesses(qm.Where("token=?", token)).One(ctx, p.db)
	// err no row means user was not found
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("token", merror.DVNotFound)
	}
	return user, err
}
