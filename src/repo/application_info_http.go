package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// HTTP implements ApplicationInfo repository interface using HTTP REST
type ApplicationInfoHTTP struct {
	rester rester.Client
}

// NewApplicationInfoHTTP is ApplicationInfoHTTP constructor
func NewApplicationInfoHTTP(rester rester.Client) *ApplicationInfoHTTP {
	return &ApplicationInfoHTTP{
		rester: rester,
	}
}

// Get
func (aih *ApplicationInfoHTTP) Get(ctx context.Context, appID string) (model.ApplicationInfo, error) {
	route := fmt.Sprintf("/application-info/%s", appID)
	appInfo := model.ApplicationInfo{}

	err := aih.rester.Get(ctx, route, nil, &appInfo)

	return appInfo, err
}
