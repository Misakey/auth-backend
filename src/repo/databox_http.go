package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/rester"
)

// HTTP implements Databox repository interface using HTTP REST
type DataboxHTTP struct {
	rester rester.Client
}

// NewDataboxHTTP is HTTP databox structure constructor
func NewDataboxHTTP(rester rester.Client) *DataboxHTTP {
	return &DataboxHTTP{
		rester: rester,
	}
}

func (dh *DataboxHTTP) Delete(ctx context.Context, userID string) error {
	route := fmt.Sprintf("/users/%s", userID)

	err := dh.rester.Delete(ctx, route, nil)
	if err != nil {
		return err
	}
	return nil
}
