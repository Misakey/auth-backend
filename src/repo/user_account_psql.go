package repo

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/adaptor/slices"
	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserAccountPSQL manages users storage using PSQL database and sqlboiler ORM
type UserAccountPSQL struct {
	db *sql.DB
}

// NewUserAccountPSQL is UserAccountPSQL constructor
func NewUserAccountPSQL(db *sql.DB) *UserAccountPSQL {
	return &UserAccountPSQL{
		db: db,
	}
}

// Insert the user in storage, it should not overwrite a user
func (p *UserAccountPSQL) Insert(ctx context.Context, user *model.UserAccount) error {
	// generate new UUID for new record
	id, err := uuid.NewRandom()
	if err != nil {
		return merror.Transform(err).Describe("could not generate uuid v4")
	}
	user.ID = id.String()
	user.KeepAliveDuration = 2592000 // 30 days: 30 * 24 * 60 * 60

	err = user.Insert(ctx, p.db, boil.Infer())
	if err != nil {
		// try to consider pq error for better return accuracy
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" {
			switch pqErr.Constraint {
			case "user_account_email_key":
				return merror.Conflict().Describe(err.Error()).Detail("email", merror.DVConflict)
			case "user_account_handle_key":
				return merror.Conflict().Describe(err.Error()).Detail("handle", merror.DVConflict)
			}
		}
	}
	return err
}

func (p *UserAccountPSQL) Get(ctx context.Context, id string) (*model.UserAccount, error) {
	user, err := model.UserAccounts(qm.Where("id=?", id), qm.Where("to_delete=?", false)).One(ctx, p.db)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("id", merror.DVNotFound)
	}
	return user, err
}

// GetByEmail will look up the user based on the passed email
func (p *UserAccountPSQL) GetByEmail(ctx context.Context, email string, includeToDelete bool) (*model.UserAccount, error) {
	mods := []qm.QueryMod{}
	mods = append(mods, qm.Where("email=?", email))
	if !includeToDelete {
		mods = append(mods, qm.Where("to_delete=?", false))
	}
	user, err := model.UserAccounts(mods...).One(ctx, p.db)
	// err no rows means user was not found
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("email", merror.DVNotFound)
	}
	return user, err
}

// List email sort by alphabetical order based on their ids
func (p *UserAccountPSQL) List(ctx context.Context, filters model.UserAccountFilters) ([]*model.UserAccount, error) {
	mods := []qm.QueryMod{}
	if len(filters.IDs) > 0 {
		mods = append(mods, qm.WhereIn("id IN ?", slices.StringSliceToInterfaceSlice(filters.IDs)...))
	}

	// email sort by alphabetical order
	mods = append(mods, qm.OrderBy("email ASC"))

	users, err := model.UserAccounts(mods...).All(ctx, p.db)
	if err == nil && users == nil {
		return []*model.UserAccount{}, nil
	}
	return users, err
}

// Update persists the user in the database
// parameter "backup" can be "nil" to indicate that backup need not be updated
func (p *UserAccountPSQL) Update(ctx context.Context, user *model.UserAccount, backup *model.Backup) error {
	tx, err := p.db.BeginTx(ctx, nil)
	if err != nil {
		tx.Rollback()
		return err
	}

	rowsAff, err := user.Update(ctx, tx, boil.Infer())
	if err != nil {
		tx.Rollback()
		return err
	}

	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		tx.Rollback()
		return merror.NotFound().Describe("no rows affected while updating user record").Detail("id", merror.DVNotFound)
	}

	if backup != nil {
		rowsAff, err := backup.Update(ctx, tx, boil.Infer())
		if err != nil {
			tx.Rollback()
			return err
		}

		// if 0 rows has been affected it means record was not found
		if rowsAff == 0 {
			tx.Rollback()
			return merror.NotFound().Describe("no row affected").Detail("id", merror.DVNotFound)
		}
	}

	return tx.Commit()
}

func (p *UserAccountPSQL) PartialUpdate(ctx context.Context, user *model.UserAccount, fields ...string) error {
	rowsAff, err := user.Update(ctx, p.db, boil.Whitelist(fields...))
	if err != nil {
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "invalid_text_representation" {
			return merror.New(merror.BadRequestError, merror.BadRequestCode, err.Error())
		}
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected").Detail("id", merror.DVNotFound)
	}
	return err
}

func (p *UserAccountPSQL) Delete(ctx context.Context, user *model.UserAccount) error {
	user.ToDelete = true

	err := p.Update(ctx, user, nil)
	if err != nil {
		return err
	}
	return nil
}

// BulkHardDelete takes a list of user accounts IDs and hard deletes them
func (p *UserAccountPSQL) BulkHardDelete(ctx context.Context, IDs []string) error {
	rowsAff, err := model.UserAccounts(qm.WhereIn(`id in ?`, slices.StringSliceToInterfaceSlice(IDs)...)).DeleteAll(ctx, p.db)
	if err != nil {
		return err
	}
	if rowsAff == 0 {
		return merror.NotFound().Describe("no rows affected")
	}
	return nil
}
