package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/rester"
)

// ConsentHTTP interacts with consent service over HTTP requests.
type ConsentHTTP struct {
	rester rester.Client
}

func NewConsentHTTP(rester rester.Client) *UserConsentHTTP {
	return &UserConsentHTTP{
		rester: rester,
	}
}

// Delete
func (uch *UserConsentHTTP) Delete(ctx context.Context, userID string) error {
	route := fmt.Sprintf("/users/%s", userID)
	return uch.rester.Delete(ctx, route, nil)
}
