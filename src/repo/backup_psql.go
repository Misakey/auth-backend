package repo

import (
	"context"
	"database/sql"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// BackupPSQL manages user backup storage using PSQL database and sqlboiler ORM
type BackupPSQL struct {
	db *sql.DB
}

// NewBackupPSQL is BackupPSQL constructor
func NewBackupPSQL(db *sql.DB) *BackupPSQL {
	return &BackupPSQL{
		db: db,
	}
}

// Insert a backup
func (p *BackupPSQL) Insert(ctx context.Context, backup *model.Backup) error {
	return backup.Insert(ctx, p.db, boil.Infer())
}

// GetByUserID will look up the backup based on the passed userID
func (p *BackupPSQL) GetByUserID(ctx context.Context, userID string) (*model.Backup, error) {
	user, err := model.Backups(qm.Where("user_id=?", userID), qm.Where("to_delete=?", false)).One(ctx, p.db)

	// err no row means no backup has been found
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().Describe(err.Error()).Detail("user_id", merror.DVNotFound)
	}
	return user, err
}

// Update persists the backup in the database
func (p *BackupPSQL) Update(ctx context.Context, backup *model.Backup) error {
	rowsAff, err := backup.Update(ctx, p.db, boil.Infer())
	if err != nil {
		return err
	}

	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		return merror.NotFound().Describe("no row affected").Detail("id", merror.DVNotFound)
	}

	return nil
}
