package repo

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// HTTP implements Hydra repository interface using HTTP REST
type HydraHTTP struct {
	publicRester         rester.Client
	formPublicRester     rester.Client
	adminRester          rester.Client
	formAdminHydraRester rester.Client
}

// NewHydraHTTP is HTTP hydra structure constructor
func NewHydraHTTP(
	publicRester rester.Client,
	formPublicRester rester.Client,
	adminRester rester.Client,
	formAdminHydraRester rester.Client,
) *HydraHTTP {
	return &HydraHTTP{
		publicRester:         publicRester,
		formPublicRester:     formPublicRester,
		adminRester:          adminRester,
		formAdminHydraRester: formAdminHydraRester,
	}
}

// LoginInfo retrieves information about a login request
func (h *HydraHTTP) LoginInfo(ctx context.Context, challenge string) (*model.LoginInfo, error) {
	logInfo := model.LoginInfo{}
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/login?%s", params.Encode())
	err := h.adminRester.Get(ctx, route, nil, &logInfo)
	if err != nil {
		return nil, err
	}
	return &logInfo, nil
}

// LoginAccept confirms to Hydra valid user's credentials have been sent
func (h *HydraHTTP) LoginAccept(ctx context.Context, challenge string, accReq model.LoginAcceptRequest) (model.LoginAcceptInfo, error) {
	accInfo := model.LoginAcceptInfo{}
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/login/accept?%s", params.Encode())
	err := h.adminRester.Put(ctx, route, nil, accReq, &accInfo)
	if err != nil {
		return accInfo, err
	}
	return accInfo, nil
}

// RejectLogin informs Hydra wrong user's credentials have been sent
func (h *HydraHTTP) LoginReject(ctx context.Context, challenge string, rejectReq model.RejectRequest) error {
	params := url.Values{}
	params.Add("challenge", challenge)
	route := fmt.Sprintf("/oauth2/auth/requests/login/reject?%s", params.Encode())
	return h.adminRester.Put(ctx, route, nil, rejectReq, nil)
}

// Logout : invalidates a subject's authentication session
func (h *HydraHTTP) Logout(ctx context.Context, id string) error {
	route := fmt.Sprintf("/oauth2/auth/sessions/login?subject=%s", id)

	return h.adminRester.Delete(ctx, route, nil)
}

// RevokeToken : invalidate access & refresh tokens
func (h *HydraHTTP) RevokeToken(ctx context.Context, revocation model.TokenRevocation) error {
	params := url.Values{}
	params.Add("token", revocation.Token)
	params.Add("client_id", revocation.ClientID)
	params.Add("client_secret", revocation.ClientSecret)
	return h.formPublicRester.Post(ctx, "/oauth2/revoke", nil, params, nil)
}

func (h *HydraHTTP) Introspect(ctx context.Context, opaqueTok string) (*model.IntrospectedToken, error) {
	introTok := model.IntrospectedToken{}
	route := fmt.Sprintf("/oauth2/introspect")

	params := url.Values{}
	params.Add("token", opaqueTok)

	err := h.formAdminHydraRester.Post(ctx, route, nil, params, &introTok)
	if err != nil {
		return nil, err
	}
	return &introTok, nil
}

// CreateClient: create a new Hydra Client in hydra
func (h *HydraHTTP) CreateClient(ctx context.Context, hydraClient *model.HydraClient) error {
	route := fmt.Sprintf("/clients")

	return h.adminRester.Post(ctx, route, nil, hydraClient, nil)
}

// GetClient: retrieve a Hydra Client from hydra using a client id.
func (h *HydraHTTP) GetClient(ctx context.Context, id string) (*model.HydraClient, error) {
	cli := model.HydraClient{}
	route := fmt.Sprintf("/clients/%s", id)

	err := h.adminRester.Get(ctx, route, nil, &cli)
	if err != nil {
		return nil, merror.Transform(err).If(merror.NotFound()).Detail("id", merror.DVNotFound).End()
	}
	return &cli, nil
}

// UpdateClient: update Hydra Client in hydra
func (h *HydraHTTP) UpdateClient(ctx context.Context, hydraClient *model.HydraClient) error {
	route := fmt.Sprintf("/clients/%s", hydraClient.ID)

	return h.adminRester.Put(ctx, route, nil, hydraClient, hydraClient)
}
