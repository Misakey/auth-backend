package repo

import (
	"context"
	"fmt"
	"net/url"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

type SessionAmazonS3 struct {
	bucket       string
	avatarDomain string
	session      *s3.S3
	uploader     *s3manager.Uploader
}

// NewAvatarAmazonS3 init an S3 session
func NewAvatarAmazonS3(region, bucket string, avatarDomain string) (*SessionAmazonS3, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)
	if err != nil {
		return nil, fmt.Errorf("could not create aws session (%v)", err)
	}

	// Create S3 service client
	svc := s3.New(sess)

	uploader := s3manager.NewUploader(sess)

	s := &SessionAmazonS3{
		bucket:       bucket,
		avatarDomain: avatarDomain,
		session:      svc,
		uploader:     uploader,
	}

	return s, nil
}

func (s *SessionAmazonS3) UploadAvatar(ctx context.Context, au *model.AvatarUpload) (string, error) {
	uo, err := s.uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.bucket),
		Key:    aws.String("profile/picture/" + au.Filename),
		Body:   au.Data,
	})
	if err != nil {
		return "", merror.Internal().Describef("unable to upload %q to %q, %v", au.Filename, s.bucket, err)
	}

	logger.FromCtx(ctx).Info().Msgf("successfully uploaded %q to %q\n", au.Filename, s.bucket)

	avatarURI, _ := url.Parse(uo.Location)
	avatarURI.Host = s.avatarDomain
	avatarURI.Path = strings.TrimPrefix(avatarURI.Path, "/"+s.bucket)

	return avatarURI.String(), nil
}

func (s *SessionAmazonS3) DeleteAvatar(ad *model.AvatarDelete) error {
	err := s.session.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(s.bucket),
		Key:    aws.String(ad.Filename),
	})
	if err != nil {
		aerr, ok := err.(awserr.Error)
		if ok {
			if aerr.Code() == "NoSuchKey" {
				return merror.NotFound().Describe(err.Error())
			}
		}
		return merror.Transform(err).Describef("unable to delete object %q from %q, %v", ad.Filename, s.bucket, err)
	}
	return nil
}
