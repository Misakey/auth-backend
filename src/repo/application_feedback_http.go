package repo

import (
	"context"
	"fmt"

	"gitlab.com/Misakey/msk-sdk-go/rester"
)

// HTTP implements ApplicationFeedback repository interface using HTTP REST
type ApplicationFeedbackHTTP struct {
	rester rester.Client
}

// NewApplicationFeedbackHTTP is HTTP application feedback structure constructor
func NewApplicationFeedbackHTTP(rester rester.Client) *ApplicationFeedbackHTTP {
	return &ApplicationFeedbackHTTP{
		rester: rester,
	}
}

func (afh *ApplicationFeedbackHTTP) Delete(ctx context.Context, userID string) error {
	route := fmt.Sprintf("/users/%s", userID)

	err := afh.rester.Delete(ctx, route, nil)
	if err != nil {
		return err
	}
	return nil
}
