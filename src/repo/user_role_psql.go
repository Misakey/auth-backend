package repo

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserRolePSQL manages user role using PSQL database and sqlboiler ORM
type UserRolePSQL struct {
	db *sql.DB
}

// NewUserRolePSQL is UserRolePSQL constructor
func NewUserRolePSQL(db *sql.DB) *UserRolePSQL {
	return &UserRolePSQL{
		db: db,
	}
}

// Insert a User Role
func (p *UserRolePSQL) Insert(ctx context.Context, userRole *model.UserRole) error {
	err := userRole.Insert(ctx, p.db, boil.Infer())
	if err != nil {
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if ok && pqErr.Code.Name() == "unique_violation" && pqErr.Constraint == "user_role_user_id_role_label_application_id_key" ||
			pqErr.Constraint == "user_role_role_label_fkey" {
			return merror.Conflict().Describe(err.Error()).
				Detail("role_label", merror.DVConflict).
				Detail("user_id", merror.DVConflict).
				Detail("application_id", merror.DVConflict)
		}
	}
	return err
}

// List User Role based on optional filters
func (p *UserRolePSQL) List(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
	mods := []qm.QueryMod{}
	if filters.UserID != "" {
		mods = append(mods, qm.Where("user_id = ?", filters.UserID))
	}
	if filters.ApplicationID != "" {
		mods = append(mods, qm.Where("application_id = ?", filters.ApplicationID))
	}
	if filters.RoleLabel != "" {
		mods = append(mods, qm.Where("role_label = ?", filters.RoleLabel))
	}

	userRoles, err := model.UserRoles(mods...).All(ctx, p.db)
	if err != nil {
		return nil, err
	}

	if userRoles == nil {
		userRoles = []*model.UserRole{}
	}

	return userRoles, nil
}
