package repo

import (
	"context"
	"net/url"
	"strings"

	"gitlab.com/Misakey/msk-sdk-go/rester"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// UserConsentHTTP implements UserConsent repository interface using HTTP REST
type UserConsentHTTP struct {
	rester rester.Client
}

func NewUserConsentHTTP(rester rester.Client) *UserConsentHTTP {
	return &UserConsentHTTP{
		rester: rester,
	}
}

// List
func (uch *UserConsentHTTP) List(ctx context.Context, filters *model.UserConsentFilters) ([]model.UserConsent, error) {
	userConsents := []model.UserConsent{}

	params := url.Values{}
	params.Add("user_id", filters.UserID)
	params.Add("application_ids", strings.Join(filters.ApplicationIDs, ","))

	err := uch.rester.Get(ctx, "/user-consents", params, &userConsents)
	return userConsents, err
}
