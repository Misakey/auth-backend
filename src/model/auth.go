package model

// Models used for auth flows

// CodeFlowParams is used as parameter for authorization code flow request
// it is built from query parameters
type CodeFlowParams struct {
	Scopes []string
	State  string
}

// TokenFlowParams is used as parameter for token flow request
// it is built from query parameters
type TokenFlowParams struct {
	Code   string
	Scopes []string
	State  string
}
