package model

// AuthRequest defines input from client about an authentication request.
type AuthRequest struct {
	Email     string `json:"email" validate:"required"`
	Password  string `json:"password" validate:"required"`
	Challenge string `json:"challenge" validate:"required"`
}

// LoginAcceptInfo contains data about what to do after login success (from Hydra)
type LoginAcceptInfo struct {
	RedirectTo string `json:"redirect_to"`
}

// LoginInfo contains data about user authentication status (from Hydra)
type LoginInfo struct {
	Skip           bool            `json:"skip"`
	Subject        string          `json:"subject"`
	Info           HydraClientInfo `json:"client"`
	RequestedScope []string        `json:"requested_scope"`
}

// LoginAcceptRequest contains data about user authentication acceptance (to Hydra)
type LoginAcceptRequest struct {
	Acr         string `json:"acr"`
	Subject     string `json:"subject"`
	Remember    bool   `json:"remember"`
	RememberFor int    `json:"remember_for"`
}

// RejectRequest contains data about user login rejection (to Hydra)
type RejectRequest struct {
	Error            string `json:"error"`
	ErrorDebug       string `json:"error_debug"`
	ErrorDescription string `json:"error_description"`
	ErrorHint        string `json:"error_hint"`
	StatusCode       int    `json:"status_code"`
}

// LogoutRequest contains the id of the user
type LogoutRequest struct {
	UserID string `json:"user_id" validate:"required"`
}
