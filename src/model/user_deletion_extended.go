package model

import (
	"time"

	"github.com/volatiletech/null"
)

// Filters for UserDeletion listing
type UserDeletionFilters struct {
	Notified      null.Bool
	UpdatedBefore time.Time
	CreatedBefore time.Time
}
