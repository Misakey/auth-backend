package model

import (
	"io"
)

// AvatarUpload represents the avatar object from the upload action
// Validation rules and bindings are done manually.
type AvatarUpload struct {
	UserID   string    `json:"user_id"`
	Filename string    `json:"file_name"`
	Size     int64     `json:"size"`
	Data     io.Reader `json:"data"`
}

// AvatarUpload represents the avatar object from the upload action
// Validation rules and bindings are done manually.
type AvatarDelete struct {
	UserID   string `json:"user_id"`
	Filename string `json:"file_name"`
}
