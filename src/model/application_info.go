package model

type ApplicationInfo struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	MainDomain string `json:"main_domain"`
	LogoURI    string `json:"logo_uri"`
	ShortDesc  string `json:"short_desc"`
	LongDesc   string `json:"long_desc"`
	DPOEmail   string `json:"dpo_email"`
}
