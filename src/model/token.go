package model

// Retrieved Data from Hydra access token introspection
type IntrospectedToken struct {
	Audiences []string `json:"aud"`
	ClientID  string   `json:"client_id"`
	Scope     string   `json:"scope"`
	Subject   string   `json:"sub"`

	// fields validated by Hydra
	Issuer    string `json:"iss"`
	ExpiresAt int64  `json:"exp"`
	IssuedAt  int64  `json:"iat"`
	NotBefore int64  `json:"nbf"`

	// fields validated inside this service
	Active    bool   `json:"active"`
	TokenType string `json:"token_type"`
}

// Used to perform token introspection
type TokenIntropection struct {
	Token string
}

// Used to perform token revocation
type TokenRevocation struct {
	// composed credential since require data to revoke the token
	Credentials
	Token string
}
