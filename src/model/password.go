package model

// PasswordUpdate represents the action of a user updating their password
type PasswordUpdate struct {
	UserID string `json:"user_id" validate:"required"`
	Old    string `json:"old_password" validate:"required"`
	New    string `json:"new_password" validate:"required"`
	// TODO set to required when implemented on frontend side
	BackupData string `json:"backup_data"`
}

// PasswordReset represents the action of resetting a user password
// XXX password reset currently makes all user secrets unusable
// because user secret backup is not updated
// (need social key backup mecanism to be working again)
type PasswordReset struct {
	Email string `json:"email" validate:"required"`
	New   string `json:"new_password" validate:"required"`
	OTP   string `json:"otp" validate:"required"`
}

// PasswordConfirmOTP represents the action of confirming the password OTP
type PasswordConfirmOTP struct {
	Email string `json:"email" validate:"required"`
	OTP   string `json:"otp" validate:"required"`
}

// AskPasswordReset represents the action of asking for a new password
type AskPasswordReset struct {
	Email string `json:"email" validate:"required,email"`
}
