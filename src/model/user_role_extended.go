package model

// UserRoleFilters used to filter user roles on list methods
type UserRoleFilters struct {
	UserID        string `validate:"required,uuid"`
	ApplicationID string
	RoleLabel     string
}
