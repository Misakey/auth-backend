package model

// Email's content and configuration
type Email struct {
	To   string
	From string

	Subject string

	HTMLBody string
	TextBody string
}
