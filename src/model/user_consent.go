package model

type UserConsentFilters struct {
	UserID         string
	ApplicationIDs []string
}

// UserConsent is an object representing a user consent to an application purpose
type UserConsent struct {
	ID int `json:"id"`
}
