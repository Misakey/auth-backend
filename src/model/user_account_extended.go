package model

// Filters for UserAccount listing
type UserAccountFilters struct {
	IDs []string `validate:"dive,uuid"`
}

// UserAccountAskConfirm is an object representing the
// parameters sent by the the client to ask a new otp confirm
type UserAccountAskConfirm struct {
	Email string `boil:"email" json:"email" validate:"required,email"`
}

// UserAccountConfirmOTP is an object representing the
// OTP account confirmation flow
type UserAccountConfirmOTP struct {
	Email string `boil:"email" json:"email" validate:"required,email"`
	OTP   string `boil:"otp" json:"otp" validate:"required"`
}

// UserPublicInfo is an object representing the public information
// of an user, sent in response to readByEmail
type UserPublicInfo struct {
	DisplayName string `json:"display_name"`
	Avatar      string `json:"avatar_uri"`
	Handle      string `json:"handle"`
}
