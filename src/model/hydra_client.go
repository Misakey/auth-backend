package model

type HydraClient struct {
	ID string `json:"client_id" validate:"required,uuid"`

	Name    string `json:"client_name"`
	LogoURI string `json:"logo_uri"`

	Scope         string   `json:"scope"`
	GrantTypes    []string `json:"grant_types"`
	RedirectURIs  []string `json:"redirect_uris" validate:"required"`
	ResponseTypes []string `json:"response_types"`

	Audience           []string `json:"audience"`
	AllowedCorsOrigins []string `json:"allowed_cors_origins" validate:"required"`

	SubjectType               string `json:"subject_type"`
	UserinfoSignedResponseALG string `json:"userinfo_signed_response_ald"`
	TokenEndpointAuthMethod   string `json:"token_endpoint_auth_method"`
	Secret                    string `json:"client_secret"`
	SecretExpiresAt           int    `json:"client_secret_expires_at"`
}

// It represents user's updatable fields.
type HydraClientPatch struct {
	ID                 string
	RedirectURIs       []string `boil:"redirect_uris" json:"redirect_uris" validate:"required_without=AllowedCorsOrigins"`
	AllowedCorsOrigins []string `boil:"allowed_cors_origins" json:"allowed_cors_origins" validate:"required_without=RedirectURIs"`
}

type Credentials struct {
	ClientID     string
	ClientSecret string
}

type HydraRequestInfo struct {
	Client HydraClientInfo `json:"client"`
}

type HydraClientInfo struct {
	AppName  string `json:"client_name"`
	ClientID string `json:"client_id"`
	LogoURI  string `json:"logo_uri"`
}

// type Jwks struct {
// 	Keys []Keys `json:"keys"`
// }
//
// type Keys struct {
// 	Alg string   `json:"alg"`
// 	Crv string   `json:"crv"`
// 	D   string   `json:"d"`
// 	Dp  string   `json:"dp"`
// 	Dq  string   `json:"dq"`
// 	E   string   `json:"e"`
// 	K   string   `json:"k"`
// 	Kid string   `json:"kid"`
// 	Kty string   `json:"kty"`
// 	N   string   `json:"n"`
// 	P   string   `json:"p"`
// 	Q   string   `json:"q"`
// 	Qi  string   `json:"qi"`
// 	Use string   `json:"use"`
// 	X   string   `json:"x"`
// 	X5C []string `json:"x5c"`
// 	Y   string   `json:"y"`
// }

// NOTE: on purpose ignored fields
// SectorIdentifierURI string `json:"sector_identifier_uri"`
// SubjectType         string `json:"subject_type"`
// Owner     string   `json:"owner"`
// Contacts  []string `json:"contacts"`
// ClientURI string   `json:"client_uri"`
// TosURI    string   `json:"tos_uri"`
// PolicyURI string   `json:"policy_uri"`

// Jwks                      Jwks     `json:"jwks"`
// JwksURI                   string   `json:"jwks_uri"`
// RequestObjectSigningAlg   string   `json:"request_object_signing_alg"`
// RequestUris               []string `json:"request_uris"`
// TokenEndpointAuthMethod   string   `json:"token_endpoint_auth_method"`
// UserinfoSignedResponseAlg string   `json:"userinfo_signed_response_alg"`
