Votre compte sera définitivement supprimé dans 15 jours à partir du {{.date}}.

Au revoir.

Si vous n’avez pas demandé la suppression de votre compte, ou si vous souhaitez annuler sa suppression, envoyez un email à love@misakey.com pour nous le signaler. 
