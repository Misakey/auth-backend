package http

import (
	"context"
	"net/url"

	"gitlab.com/Misakey/msk-sdk-go/rester/http"
)

// PostFormClient represents a HTTP REST API client.
// this special client performs request using form format
type PostFormClient struct {
	http.Client
}

// NewPostFormClient is PostFormClient constructor
func NewPostFormClient(url string, secure bool) *PostFormClient {
	cli := &PostFormClient{
		Client: *http.NewClient(url, secure),
	}
	return cli
}

// Post performs a POST request, input is considered as form structure
func (r *PostFormClient) Post(ctx context.Context, route string, params url.Values, input interface{}, output interface{}) error {
	return r.Perform(ctx, "POST", route, params, input, output, http.FORM_FORMAT)
}
