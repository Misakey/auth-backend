package sql

//go:generate sqlboiler --wipe psql

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

// NewPostgreConn initiates a new connection to a postgres server
func NewPostgreConn(dbURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		return nil, fmt.Errorf("could not open conn to postgresql (%v)", err)
	}

	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("could not ping database (%v)", err)
	}
	return db, nil
}
