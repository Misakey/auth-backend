package service

import (
	"time"
)

type CleanupManager struct {

	// Resource repositories section
	accounts      userAccountRepo
	userDeletions userDeletionRepo

	// Emails configuration
	emailRenderer renderer
	mailer        mailerRepo

	// time before definitely deleting resources
	timeBeforeDeletion time.Duration
}

func NewCleanupManager(
	emailRenderer renderer, mailer mailerRepo, accounts userAccountRepo,
	userDeletions userDeletionRepo, timeBeforeDeletion time.Duration,
) *CleanupManager {
	return &CleanupManager{
		emailRenderer: emailRenderer,
		mailer:        mailer,

		accounts:      accounts,
		userDeletions: userDeletions,

		timeBeforeDeletion: timeBeforeDeletion,
	}
}
