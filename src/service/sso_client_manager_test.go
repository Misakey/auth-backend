package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

type ssoClientsMock struct {
	create func(*model.HydraClient) error
	get    func(context.Context, string) (*model.HydraClient, error)
	update func(*model.HydraClient) error
}

func (m ssoClientsMock) CreateClient(ctx context.Context, hydraClient *model.HydraClient) error {
	return m.create(hydraClient)
}
func (m ssoClientsMock) GetClient(ctx context.Context, id string) (*model.HydraClient, error) {
	return m.get(ctx, id)
}
func (m ssoClientsMock) UpdateClient(ctx context.Context, hydraClient *model.HydraClient) error {
	return m.update(hydraClient)
}

type applicationsMock struct {
	get func(context.Context, string) (model.ApplicationInfo, error)
}

func (m applicationsMock) Get(ctx context.Context, appID string) (model.ApplicationInfo, error) {
	return m.get(ctx, appID)
}

//
func TestCreateOrUpdateClient(t *testing.T) {
	tests := map[string]struct {
		acc          *ajwt.AccessClaims
		inputClient  *model.HydraClient
		appGet       func(context.Context, string) (model.ApplicationInfo, error)
		clientCreate func(*model.HydraClient) error
		clientGet    func(context.Context, string) (*model.HydraClient, error)
		clientUpdate func(*model.HydraClient) error
		expectedErr  error
	}{
		"a user without jwt create a new hydra client - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			inputClient: &model.HydraClient{ID: "42"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope create a new hydra client - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			inputClient: &model.HydraClient{ID: "42"},
			expectedErr: merror.Forbidden(),
		},
		"a user without admin role scope - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user", Role: "99:1"},
			inputClient: &model.HydraClient{ID: "42"},
			expectedErr: merror.Forbidden(),
		},
		"an allowed user creates a new hydra client": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user", Role: "42:1"},
			inputClient: &model.HydraClient{ID: "42", RedirectURIs: []string{"new uri"}},
			appGet: func(_ context.Context, _ string) (model.ApplicationInfo, error) {
				app := model.ApplicationInfo{ID: "42", LogoURI: "http://logo.png"}
				return app, nil
			},
			clientGet: func(_ context.Context, id string) (*model.HydraClient, error) {
				return nil, merror.NotFound()
			},
			clientCreate: func(client *model.HydraClient) error {
				situation := "creation of client"
				assert.Equalf(t, "42", client.ID, situation)
				assert.Equalf(t, "http://logo.png", client.LogoURI, situation)
				assert.Equalf(t, []string{"new uri"}, client.RedirectURIs, situation)
				return nil
			},
			expectedErr: nil,
		},
		"an allowed user updates an exisiting hydra client": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user", Role: "42:1"},
			inputClient: &model.HydraClient{ID: "42", RedirectURIs: []string{"new uri"}},
			appGet: func(_ context.Context, _ string) (model.ApplicationInfo, error) {
				app := model.ApplicationInfo{ID: "42", LogoURI: "http://logo.png"}
				return app, nil
			},
			clientGet: func(_ context.Context, id string) (*model.HydraClient, error) {
				situation := "get of client"
				assert.Equalf(t, "42", id, situation)
				return &model.HydraClient{ID: "42", RedirectURIs: []string{"old uri"}}, nil
			},
			clientUpdate: func(client *model.HydraClient) error {
				situation := "creation of client"
				assert.Equalf(t, "42", client.ID, situation)
				assert.Equalf(t, "http://logo.png", client.LogoURI, situation)
				assert.Equalf(t, []string{"new uri"}, client.RedirectURIs, situation)
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := SSOClientManager{
				ssoClients: &ssoClientsMock{
					create: test.clientCreate,
					get:    test.clientGet,
					update: test.clientUpdate,
				},
				applications: &applicationsMock{get: test.appGet},
			}

			// call function to test
			err := service.Create(ctx, test.inputClient)

			// check assertions
			if test.expectedErr != nil {
				assert.Equal(t, test.expectedErr.Error(), err.Error())
			}
		})
	}
}

//
func TestGenerateSecret(t *testing.T) {
	tests := map[string]struct {
		acc            *ajwt.AccessClaims
		appID          string
		clientGet      func(context.Context, string) (*model.HydraClient, error)
		clientUpdate   func(_ *model.HydraClient) error
		expectedErr    error
		expectedSecret string
	}{
		"a user without jwt generate a new hydra client secret - forbidden": {
			expectedErr: merror.Forbidden().Describe("missing user scope or admin role"),
		},
		"a user without user scope generate a new hydra client secret - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			expectedErr: merror.Forbidden().Describe("missing user scope or admin role"),
		},
		"a user without admin role scope - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			appID:       "42",
			expectedErr: merror.Forbidden().Describe("missing user scope or admin role"),
		},
		"a user with admin role try to generate new secret on MKapp - forbidden": {
			acc:   &ajwt.AccessClaims{Subject: "1", Scope: "user rol.admin.c001d00d-5ecc-beef-ca4e-b00b1e54a111"},
			appID: "c001d00d-5ecc-beef-ca4e-b00b1e54a111",
			clientGet: func(_ context.Context, appID string) (*model.HydraClient, error) {
				assert.Equalf(t, "c001d00d-5ecc-beef-ca4e-b00b1e54a111", appID, "client get")
				return &model.HydraClient{ID: "c001d00d-5ecc-beef-ca4e-b00b1e54a111"}, nil
			},
			expectedErr: merror.Forbidden().Describef("can't regenerate secret for c001d00d-5ecc-beef-ca4e-b00b1e54a111"),
		},
		"a user with admin role try to generate new secret on Misadmin - forbidden": {
			acc:   &ajwt.AccessClaims{Subject: "1", Scope: "user rol.admin.9e9f68c2-a3b7-491a-9d04-3144892aba1a"},
			appID: "9e9f68c2-a3b7-491a-9d04-3144892aba1a",
			clientGet: func(_ context.Context, appID string) (*model.HydraClient, error) {
				assert.Equalf(t, "9e9f68c2-a3b7-491a-9d04-3144892aba1a", appID, "client get")
				return &model.HydraClient{ID: "9e9f68c2-a3b7-491a-9d04-3144892aba1a"}, nil
			},
			expectedErr: merror.Forbidden().Describef("can't regenerate secret for 9e9f68c2-a3b7-491a-9d04-3144892aba1a"),
		},
		"a user with user scope generate a new hydra client secret- allowed": {
			acc:   &ajwt.AccessClaims{Subject: "1", Scope: "user rol.admin.42"},
			appID: "42",
			clientGet: func(_ context.Context, appID string) (*model.HydraClient, error) {
				assert.Equalf(t, "42", appID, "client get")
				return &model.HydraClient{ID: "42"}, nil
			},
			clientUpdate: func(client *model.HydraClient) error {
				desc := "client update"
				assert.Equalf(t, "42", client.ID, desc)
				assert.NotEmpty(t, client.Secret, desc)
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := SSOClientManager{
				ssoClients: &ssoClientsMock{
					get:    test.clientGet,
					update: test.clientUpdate,
				},
				applicationClientID: "c001d00d-5ecc-beef-ca4e-b00b1e54a111",
				misadminClientID:    "9e9f68c2-a3b7-491a-9d04-3144892aba1a",
			}

			// call function to test
			_, err := service.GenerateSecret(ctx, test.appID)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
