package service

import (
	"context"

	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// AddBackup add user backup to storage or update if already exists
func (am *AuthManager) AddBackup(ctx context.Context, backup *model.Backup) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(backup.UserID) {
		return merror.Forbidden().Describe("missing user scope or wrong subject")
	}

	oldBackup, err := am.backups.GetByUserID(ctx, backup.UserID)
	if merror.HasCode(err, merror.NotFoundCode) {
		return am.backups.Insert(ctx, backup)
	}

	backup.ID = oldBackup.ID
	backup.CreatedAt = oldBackup.CreatedAt

	return am.backups.Update(ctx, backup)
}

// RetrieveBackup get a user backup from storage
func (am *AuthManager) RetrieveBackup(ctx context.Context, userID string) (*model.Backup, error) {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(userID) {
		return nil, merror.Forbidden().Describe("missing user scope or wrong subject")
	}

	backup, err := am.backups.GetByUserID(ctx, userID)
	if err != nil {
		return nil, err
	}
	return backup, nil
}
