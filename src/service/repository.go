package service

import (
	"context"
	"html/template"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// repositories interfaces ordered alphabetically

type applicationRepo interface {
	Get(ctx context.Context, appID string) (model.ApplicationInfo, error)
}

type avatarRepo interface {
	UploadAvatar(ctx context.Context, au *model.AvatarUpload) (string, error)
	DeleteAvatar(ad *model.AvatarDelete) error
}

type backupRepo interface {
	Insert(ctx context.Context, backup *model.Backup) error
	Update(ctx context.Context, backup *model.Backup) error
	GetByUserID(ctx context.Context, userID string) (*model.Backup, error)
}

type consentRepo interface {
	Delete(ctx context.Context, userID string) error
}

type customAccessRepo interface {
	Insert(ctx context.Context, ca *model.CustomAccess) error
	Get(ctx context.Context, token string) (*model.CustomAccess, error)
}

type databoxRepo interface {
	Delete(ctx context.Context, userID string) error
}

type feedbackRepo interface {
	Delete(ctx context.Context, userID string) error
}

type mailerRepo interface {
	Send(ctx context.Context, email *model.Email) error
}

type publicKeyRepo interface {
	Insert(ctx context.Context, backup *model.PublicKey) error
	Update(ctx context.Context, backup *model.PublicKey) error
	GetByUserID(ctx context.Context, userID string) (*model.PublicKey, error)
}

type sessionRepo interface {
	LoginInfo(ctx context.Context, challenge string) (*model.LoginInfo, error)
	LoginAccept(ctx context.Context, challenge string, acceptReq model.LoginAcceptRequest) (model.LoginAcceptInfo, error)
	Logout(ctx context.Context, id string) error
	RevokeToken(ctx context.Context, revocation model.TokenRevocation) error
}

type ssoClientRepo interface {
	CreateClient(ctx context.Context, hydraClient *model.HydraClient) error
	GetClient(ctx context.Context, id string) (*model.HydraClient, error)
	UpdateClient(ctx context.Context, hydraClient *model.HydraClient) error
}

type templateRepo interface {
	Load(name string) error
	Get(name string) (*template.Template, error)
}

type tokenRepo interface {
	Introspect(ctx context.Context, opaqueTok string) (*model.IntrospectedToken, error)
}

type userAccountRepo interface {
	Insert(ctx context.Context, user *model.UserAccount) error
	Get(ctx context.Context, key string) (*model.UserAccount, error)
	GetByEmail(ctx context.Context, email string, includeToDelete bool) (*model.UserAccount, error)
	List(ctx context.Context, filters model.UserAccountFilters) ([]*model.UserAccount, error)
	Update(ctx context.Context, user *model.UserAccount, backup *model.Backup) error
	PartialUpdate(ctx context.Context, user *model.UserAccount, fields ...string) error
	Delete(ctx context.Context, user *model.UserAccount) error
	BulkHardDelete(ctx context.Context, IDs []string) error
}

type userConsentRepo interface {
	List(ctx context.Context, filters *model.UserConsentFilters) ([]model.UserConsent, error)
}

type userRoleRepo interface {
	Insert(ctx context.Context, userRole *model.UserRole) error
	List(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error)
}

type userDeletionRepo interface {
	Insert(ctx context.Context, userDeletion *model.UserDeletion) error
	Update(ctx context.Context, userDeletion *model.UserDeletion) error
	List(ctx context.Context, filters model.UserDeletionFilters) ([]*model.UserDeletion, error)
}
