package service

import (
	"context"
	"testing"

	"gitlab.com/Misakey/auth-backend/src/model"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"
)

type accountsMock struct {
	get           func(context.Context, string) (*model.UserAccount, error)
	partialUpdate func(context.Context, *model.UserAccount, ...string) error
	list          func(context.Context, model.UserAccountFilters) ([]*model.UserAccount, error)
}

func (m accountsMock) Insert(ctx context.Context, user *model.UserAccount) error {
	return nil
}

func (m accountsMock) Get(ctx context.Context, id string) (*model.UserAccount, error) {
	return m.get(ctx, id)
}

func (m accountsMock) List(ctx context.Context, filters model.UserAccountFilters) ([]*model.UserAccount, error) {
	return m.list(ctx, filters)
}

func (m accountsMock) GetByEmail(ctx context.Context, email string, includeToDelete bool) (*model.UserAccount, error) {
	return m.get(ctx, email)
}

func (m accountsMock) Update(ctx context.Context, user *model.UserAccount, backup *model.Backup) error {
	return nil
}

func (m accountsMock) PartialUpdate(ctx context.Context, user *model.UserAccount, fields ...string) error {
	return m.partialUpdate(ctx, user, fields...)
}

func (m accountsMock) Delete(ctx context.Context, user *model.UserAccount) error {
	return nil
}

func (m accountsMock) BulkHardDelete(ctx context.Context, IDs []string) error {
	return nil
}

//
func TestRead(t *testing.T) {
	tests := map[string]struct {
		acc          *ajwt.AccessClaims
		id           string
		accountGet   func(_ context.Context, _ string) (*model.UserAccount, error)
		expectedUser *model.UserAccount
		expectedErr  error
	}{
		"a user without jwt retrieves their account - forbidden": {
			id:          "1",
			expectedErr: merror.Forbidden().Describe("missing accesses"),
		},
		"a user retrieves another user - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			id:          "2",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope retrieves their account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "1",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope retrieves another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "2",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user retrieves their account - allowed": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			id:  "1",
			accountGet: func(_ context.Context, id string) (*model.UserAccount, error) {
				assert.Equalf(t, "1", id, "account get")
				return &model.UserAccount{DisplayName: "Snowden"}, nil
			},
			expectedUser: &model.UserAccount{DisplayName: "Snowden"},
		},
		"a service retrieves any user account - allowed": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "service"},
			id:  "2",
			accountGet: func(_ context.Context, id string) (*model.UserAccount, error) {
				assert.Equalf(t, "2", id, "account get")
				return &model.UserAccount{DisplayName: "Snowden"}, nil
			},
			expectedUser: &model.UserAccount{DisplayName: "Snowden"},
		},
		"password is empty retrieving any user account": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			id:  "1",
			accountGet: func(_ context.Context, id string) (*model.UserAccount, error) {
				assert.Equalf(t, "1", id, "account get")
				return &model.UserAccount{DisplayName: "Snowden", Password: "thisisastrongpassword"}, nil
			},
			expectedUser: &model.UserAccount{DisplayName: "Snowden", Password: ""},
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{
				accounts: &accountsMock{get: test.accountGet},
			}

			// call function to test
			user, err := service.Read(ctx, test.id)

			// check assertions
			assert.Equal(t, test.expectedUser, user)
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestPartialUpdate(t *testing.T) {
	tests := map[string]struct {
		acc           *ajwt.AccessClaims
		pi            *patch.Info
		accountUpdate func(context.Context, *model.UserAccount, ...string) error
		expectedErr   error
	}{
		"a user without jwt update an account - forbidden": {
			pi:          &patch.Info{Model: &model.UserAccount{}},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope update their account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pi:          &patch.Info{Model: &model.UserAccount{ID: "2"}},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope update another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pi:          &patch.Info{Model: &model.UserAccount{ID: "2"}},
			expectedErr: merror.Forbidden(),
		},
		"a user updates its account - fields white list is correct": {
			acc: &ajwt.AccessClaims{Scope: "user", Subject: "32"},
			pi: &patch.Info{
				Input:  []string{"DisplayName", "KeepAliveDuration"},
				Output: []string{"display_name", "keep_alive_duration"},
				Model:  &model.UserAccount{ID: "32", KeepAliveDuration: 1, DisplayName: "Deden"},
			},
			accountUpdate: func(_ context.Context, account *model.UserAccount, fields ...string) error {
				desc := "account partial update"
				assert.Equalf(t, "32", account.ID, desc)
				assert.Equalf(t, "Deden", account.DisplayName, desc)
				assert.Equalf(t, 1, account.KeepAliveDuration, desc)
				assert.Equalf(t, []string{"display_name", "keep_alive_duration"}, fields, desc)
				return nil
			},
			expectedErr: nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{
				accounts: accountsMock{partialUpdate: test.accountUpdate},
			}

			// call function to test
			err := service.PartialUpdate(ctx, test.pi)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

func TestList(t *testing.T) {
	tests := map[string]struct {
		acc           *ajwt.AccessClaims
		filters       model.UserAccountFilters
		accountList   func(_ context.Context, _ model.UserAccountFilters) ([]*model.UserAccount, error)
		expectedUsers []*model.UserAccount
		expectedErr   error
	}{
		"a user without jwt list all users account - forbidden": {
			filters:     model.UserAccountFilters{IDs: []string{""}},
			expectedErr: merror.Forbidden(),
		},
		"a service list multiple user accounts - allowed": {
			acc:     &ajwt.AccessClaims{Subject: "1", Scope: "service"},
			filters: model.UserAccountFilters{IDs: []string{"1", "2"}},
			accountList: func(_ context.Context, id model.UserAccountFilters) ([]*model.UserAccount, error) {
				return []*model.UserAccount{&model.UserAccount{DisplayName: "Snowden", Password: "thisisastrongpassword"}}, nil
			},
			expectedUsers: []*model.UserAccount{&model.UserAccount{DisplayName: "Snowden", Password: ""}},
		},
		"a user list any user accounts - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters:     model.UserAccountFilters{IDs: []string{"1"}},
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{
				accounts: &accountsMock{list: test.accountList},
			}

			// call function to test
			users, err := service.List(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedUsers, users)
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestDelete(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		id          string
		expectedErr error
	}{
		"a user without jwt delete an account - forbidden": {
			id:          "1",
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope delete their account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "1",
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope delete another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "2",
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope delete another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			id:          "2",
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.Delete(ctx, test.id)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestPaswordUpdate(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		pu          *model.PasswordUpdate
		expectedErr error
	}{
		"a user without jwt change password - forbidden": {
			pu:          &model.PasswordUpdate{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope change their password - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pu:          &model.PasswordUpdate{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope change another user password - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pu:          &model.PasswordUpdate{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope change another user password - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			pu:          &model.PasswordUpdate{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.PasswordUpdate(ctx, test.pu)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestAddAvatar(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		au          *model.AvatarUpload
		repoUser    *model.UserAccount
		expectedErr error
	}{
		"a user without jwt upload avatar - forbidden": {
			au:          &model.AvatarUpload{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope upload their avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			au:          &model.AvatarUpload{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope upload another user avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			au:          &model.AvatarUpload{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope upload another user avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			au:          &model.AvatarUpload{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.AddAvatar(ctx, test.au)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestDeleteAvatar(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		ad          *model.AvatarDelete
		expectedErr error
	}{
		"a user without jwt delete avatar - forbidden": {
			ad:          &model.AvatarDelete{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope delete their avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			ad:          &model.AvatarDelete{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope delete another user avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			ad:          &model.AvatarDelete{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope delete another user avatar - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			ad:          &model.AvatarDelete{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.DeleteAvatar(ctx, test.ad)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
