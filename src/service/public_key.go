package service

import (
	"context"

	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// AddPublicKey add a new user public key to storage
func (am *AuthManager) AddPublicKey(ctx context.Context, pk *model.PublicKey) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(pk.UserID) {
		return merror.Forbidden().Describe("missing user scope or wrong subject")
	}

	return am.publicKeys.Insert(ctx, pk)
}

// RetrievePublicKey get a user public key from storage
func (am *AuthManager) RetrievePublicKey(ctx context.Context, userID string) (*model.PublicKey, error) {
	pk, err := am.publicKeys.GetByUserID(ctx, userID)
	if err != nil {
		return nil, err
	}
	return pk, nil
}
