package service

import (
	"context"
	"path/filepath"
	"time"

	"github.com/google/uuid"
	"github.com/volatiletech/null"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/auth-backend/src/service/security"
)

const (
	// generated token size for confirm, recover...
	hashCost = 10
	// in ns == 5 minutes
	otpDuration = 300000000000
)

func (am *AuthManager) Create(ctx context.Context, user *model.UserAccount) error {
	// check user can be created -> it does not exist yet
	existing, err := am.accounts.GetByEmail(ctx, user.Email, false)

	// we ignore not found error
	if err != nil && !merror.HasCode(err, merror.NotFoundCode) {
		return err
	}
	if existing != nil {
		return merror.Conflict().Describe("user email is not unique").Detail("email", merror.DVConflict)
	}

	// Generate OTP
	otp, err := security.GenerateOTP()
	if err != nil {
		return merror.Transform(err).Describe("could not generate otp")
	}

	// HashOTP
	hashedOTP, err := security.HashOTP(otp)
	if err != nil {
		return merror.Transform(err).Describe("could not hash otp")
	}

	// generate password
	user.Password, err = security.HashPassword(user.Password, hashCost)
	if err != nil {
		return err
	}

	// check handle is correctly formatted
	if !am.userHandleRegexp.Match([]byte(user.Handle)) {
		return merror.BadRequest().
			Describef("shall match %s", am.userHandleRegexp.String()).
			Detail("handle", merror.DVInvalid)
	}

	// set defaults user KeepAliveDuration
	if user.KeepAliveDuration == 0 {
		user.KeepAliveDuration = 2592000 // 30 days: 30 * 24 * 60 * 60
	}

	// set OTP and avatar uri
	user.ConfirmOtpCreatedAt = null.NewTime(time.Now().UTC(), true)
	user.ConfirmOtp = null.NewString(hashedOTP, true)
	user.AvatarURI = null.NewString("", false)
	err = am.accounts.Insert(ctx, user)
	if err != nil {
		return merror.Transform(err).If(merror.ConflictError).Describe("user is not unique").End()
	}

	// create Backup entity
	userBackup := model.Backup{}
	userBackup.Data = user.BackupData
	userBackup.UserID = user.ID

	err = am.backups.Insert(ctx, &userBackup)
	if err != nil {
		return err
	}

	// create Public Key entity
	userPublicKey := model.PublicKey{}
	userPublicKey.Pubkey = user.PublicKeyData
	userPublicKey.UserID = user.ID

	err = am.publicKeys.Insert(ctx, &userPublicKey)
	if err != nil {
		return err
	}

	// notify asynchronously user about confirmation
	go func() {
		err = am.notifyConfirm(ctx, user.Email, otp)
		if err != nil {
			logger.
				FromCtx(ctx).
				Warn().
				Err(err).
				Msgf("could not confirm to %s", user.Email)
		}
	}()

	// remove unwanted information from response
	user.Password = ""
	user.PublicKeyData = ""
	user.BackupData = ""

	return nil
}

func (am *AuthManager) Read(ctx context.Context, id string) (*model.UserAccount, error) {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	// verify requested user id and authenticated user id are the same.
	if acc == nil {
		return nil, merror.Forbidden().Describe("missing accesses")
	}

	// service bypass other security check
	if acc.IsNotAnyService() && acc.IsNotUser(id) {
		return nil, merror.Forbidden().Describe("missing user scope or wrong subject")
	}

	user, err := am.accounts.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	// /!\ do not return the password !
	user.Password = ""

	return user, nil
}

func (am *AuthManager) ReadByEmail(ctx context.Context, email string) (*model.UserPublicInfo, error) {
	user, err := am.accounts.GetByEmail(ctx, email, true)
	if err != nil {
		return nil, err
	}
	upi := &model.UserPublicInfo{
		DisplayName: user.DisplayName,
		Avatar:      user.AvatarURI.String,
		Handle:      user.Handle,
	}
	return upi, nil
}

func (am *AuthManager) PartialUpdate(ctx context.Context, pi *patch.Info) error {
	userAccount := pi.Model.(*model.UserAccount)

	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(userAccount.ID) {
		return merror.Forbidden()
	}

	// filter out fields we never want to patch
	pi.Whitelist([]string{"DisplayName", "KeepAliveDuration", "DefaultLanguage"})
	return am.accounts.PartialUpdate(ctx, userAccount, pi.Output...)
}

func (am *AuthManager) PasswordUpdate(ctx context.Context, pu *model.PasswordUpdate) error {
	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// Verify authenticated user id and access JWT user id are the same.
	if acc == nil || acc.IsNotUser(pu.UserID) {
		return merror.Forbidden()
	}

	user, err := am.accounts.Get(ctx, acc.Subject)
	if err != nil {
		return err
	}

	// compare given old password with existing one for security purpose
	_, err = security.CheckHashedPassword(pu.Old, user.Password)
	if err != nil {
		return err
	}

	// hash new password
	user.Password, err = security.HashPassword(pu.New, hashCost)
	if err != nil {
		return err
	}

	var backup *model.Backup
	// TODO: check to remove when BackupData becomes required (when implemented by frontend)
	if pu.BackupData != "" {
		backup, err = am.backups.GetByUserID(ctx, pu.UserID)
		if err != nil {
			return err
		}
		backup.Data = pu.BackupData
	}

	// save changed password
	return am.accounts.Update(ctx, user, backup)
}

// List users based on their ids
func (am *AuthManager) List(ctx context.Context, filters model.UserAccountFilters) ([]*model.UserAccount, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAnyService() {
		return nil, merror.Forbidden()
	}

	users, err := am.accounts.List(ctx, filters)
	if err != nil {
		return nil, err
	}

	for _, user := range users {
		user.Password = ""
	}

	return users, nil
}

// PasswordConfirmOTP : Verify that the OTP is equal to ours and generate a new one that act as
// a token for resetting the password
func (am *AuthManager) PasswordConfirmOTP(ctx context.Context, poc *model.PasswordConfirmOTP) (*model.PasswordConfirmOTP, error) {
	user, err := am.accounts.GetByEmail(ctx, poc.Email, false)
	if err != nil {
		return nil, err
	}

	// Check if OTP exists
	if user.ResetOtp.IsZero() {
		return nil, merror.Conflict().Describe("no password reset has been asked")
	}

	// Check OTP
	check, err := security.CheckHashedOTP(poc.OTP, user.ResetOtp.String)
	if err != nil || !check {
		return nil, merror.Forbidden().From(merror.OriBody).Describe("wrong otp").Detail("otp", merror.DVInvalid)
	}

	// Check if OTP expired
	if time.Since(user.ResetOtpCreatedAt.Time) > otpDuration {
		return nil, merror.Forbidden().From(merror.OriBody).Describe("expired otp").Detail("otp", merror.DVExpired)
	}

	// Generate OTP Reset
	otp, err := security.GenerateOTP()
	if err != nil {
		return nil, merror.Transform(err).Describe("could not generate otp")
	}

	// Hash OTP
	hashedOTP, err := security.HashOTP(otp)
	if err != nil {
		return nil, merror.Transform(err).Describe("could not hash otp")
	}

	// Add OTP RESET DB
	poc.OTP = otp
	user.ResetOtpCreatedAt = null.NewTime(time.Now().UTC(), true)
	user.ResetOtp = null.NewString(hashedOTP, true)
	err = am.accounts.Update(ctx, user, nil)
	if err != nil {
		return nil, err
	}
	return poc, nil

}

// AskPasswordReset : Generate a One Time Password (OTP) sent to the user email
func (am *AuthManager) AskPasswordReset(ctx context.Context, apr *model.AskPasswordReset) error {
	user, err := am.accounts.GetByEmail(ctx, apr.Email, false)
	if err != nil {
		return err
	}

	// Generate OTP Reset
	otp, err := security.GenerateOTP()
	if err != nil {
		return merror.Transform(err).Describe("could not generate otp")
	}

	// Hash OTP
	hashedOTP, err := security.HashOTP(otp)
	if err != nil {
		return merror.Transform(err).Describe("could not hash otp")
	}

	// Add OTP RESET DB
	user.ResetOtpCreatedAt = null.NewTime(time.Now().UTC(), true)
	user.ResetOtp = null.NewString(hashedOTP, true)
	err = am.accounts.Update(ctx, user, nil)
	if err != nil {
		return err
	}

	// Send asynchronously email with OTP @ email
	go func() {
		err = am.notifyOTP(ctx, user.Email, otp)
		if err != nil {
			logger.
				FromCtx(ctx).
				Warn().
				Err(err).
				Msgf("could not notify ask reset password to %s", user.Email)
		}
	}()

	return nil
}

// PasswordReset : Verify that the OTP is equal to ours and update the password
func (am *AuthManager) PasswordReset(ctx context.Context, pr *model.PasswordReset) error {
	user, err := am.accounts.GetByEmail(ctx, pr.Email, false)
	if err != nil {
		return err
	}

	// Check if OTP exists
	if user.ResetOtp.IsZero() {
		return merror.Conflict().Describe("no password reset has been asked")
	}

	// Check OTP
	check, err := security.CheckHashedOTP(pr.OTP, user.ResetOtp.String)
	if err != nil || !check {
		return merror.Forbidden().From(merror.OriBody).Describe("wrong otp").Detail("otp", merror.DVInvalid)
	}

	// Check if OTP expired
	if time.Since(user.ResetOtpCreatedAt.Time) > otpDuration {
		return merror.Forbidden().From(merror.OriBody).Describe("expired otp").Detail("otp", merror.DVExpired)
	}

	// hash new password
	user.ResetOtp = null.NewString("", false)
	user.ResetOtpCreatedAt = null.NewTime(time.Now().UTC(), true)
	user.Password, err = security.HashPassword(pr.New, hashCost)
	if err != nil {
		return err
	}

	// save changed password
	return am.accounts.Update(ctx, user, nil)
}

// Confirm : Confirm the user account thanks to a One Time Passwords
func (am *AuthManager) Confirm(ctx context.Context, confirmOTP *model.UserAccountConfirmOTP) error {
	user, err := am.accounts.GetByEmail(ctx, confirmOTP.Email, false)
	if err != nil {
		return err
	}

	// Check if user already confirmed
	if user.Confirmed {
		return merror.Conflict().Describe("user already confirmed")
	}

	// Check if OTP exists
	if user.ConfirmOtp.IsZero() {
		return merror.Conflict().Describe("empty confirm otp")
	}

	// Check OTP
	check, err := security.CheckHashedOTP(confirmOTP.OTP, user.ConfirmOtp.String)
	if err != nil || !check {
		return merror.Forbidden().From(merror.OriBody).Describe("wrong otp").Detail("otp", merror.DVInvalid)
	}

	// Check if OTP expired
	if time.Since(user.ConfirmOtpCreatedAt.Time) > otpDuration {
		return merror.Forbidden().From(merror.OriBody).Describe("expired otp").Detail("otp", merror.DVExpired)
	}

	// Set ConfirmOtp to nil
	user.ConfirmOtp = null.NewString("", false)
	user.ConfirmOtpCreatedAt = null.NewTime(time.Time{}, false)
	// Set user account confirmed to true
	user.Confirmed = true
	err = am.accounts.Update(ctx, user, nil)
	if err != nil {
		return err
	}
	return nil
}

// AskConfirm : Regenerate a new OTP to confirm a new account
func (am *AuthManager) AskConfirm(ctx context.Context, askOTP *model.UserAccountAskConfirm) error {
	user, err := am.accounts.GetByEmail(ctx, askOTP.Email, false)
	if err != nil {
		return err
	}

	// Check if user already confirmed
	if user.Confirmed {
		return merror.Conflict().Describe("user already confirmed")
	}

	// Generate OTP
	otp, err := security.GenerateOTP()
	if err != nil {
		return merror.Transform(err).Describe("could not generate otp")
	}

	// HashOTP
	hashedOTP, err := security.HashOTP(otp)
	if err != nil {
		return merror.Transform(err).Describe("could not hash otp")
	}

	// Insert new OTP
	user.ConfirmOtpCreatedAt = null.NewTime(time.Now().UTC(), true)
	user.ConfirmOtp = null.NewString(hashedOTP, true)
	err = am.accounts.Update(ctx, user, nil)
	if err != nil {
		return err
	}

	// Send asynchronously email with OTP @ email
	go func() {
		err = am.notifyOTP(ctx, user.Email, otp)
		if err != nil {
			logger.
				FromCtx(ctx).
				Warn().
				Err(err).
				Msgf("could not notify ask reset password to %s", user.Email)
		}
	}()

	return nil
}

func (am *AuthManager) notifyConfirm(ctx context.Context, to string, otp string) error {
	data := map[string]interface{}{
		"to":  to,
		"otp": otp,
	}
	email, err := am.emailRenderer.NewEmail(ctx, to, "Votre code de validation - Misakey", "confirm", data)
	if err != nil {
		return err
	}
	return am.mailer.Send(ctx, email)
}

func (am *AuthManager) notifyOTP(ctx context.Context, to string, otp string) error {
	data := map[string]interface{}{
		"to":  to,
		"otp": otp,
	}
	email, err := am.emailRenderer.NewEmail(ctx, to, "Votre code de validation - Misakey", "reset", data)
	if err != nil {
		return err
	}
	return am.mailer.Send(ctx, email)
}

func (am *AuthManager) notifyDeletion(ctx context.Context, to string) error {
	deletionDate := time.Now().Format("02-01-2006")
	data := map[string]interface{}{
		"to":   to,
		"date": deletionDate,
	}
	email, err := am.emailRenderer.NewEmail(ctx, to, "Votre demande de suppression de compte - Misakey", "delete", data)
	if err != nil {
		return err
	}
	return am.mailer.Send(ctx, email)
}

// AddAvatar : Upload or Update the user avatar to S3 and inster it in the DB
func (am *AuthManager) AddAvatar(ctx context.Context, au *model.AvatarUpload) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(au.UserID) {
		return merror.Forbidden()
	}

	user, err := am.accounts.Get(ctx, au.UserID)
	if err != nil {
		return err
	}

	// generate new UUID instead of default file name
	filename, err := uuid.NewRandom()
	if err != nil {
		return merror.Transform(err).Describe("could not generate uuid v4")
	}

	au.Filename = filename.String()

	if !user.AvatarURI.IsZero() {
		ad := &model.AvatarDelete{
			UserID:   au.UserID,
			Filename: au.Filename,
		}
		err = am.DeleteAvatar(ctx, ad)
		if err != nil {
			return err
		}
	}

	url, err := am.avatars.UploadAvatar(ctx, au)
	if err != nil {
		return err
	}

	user.AvatarURI = null.StringFrom(url)

	return am.accounts.Update(ctx, user, nil)
}

// DeleteAvatar : Remove the user avatar from S3 and the DB
func (am *AuthManager) DeleteAvatar(ctx context.Context, ad *model.AvatarDelete) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// verify requested user id and authenticated user id are the same.
	if acc == nil || acc.IsNotUser(ad.UserID) {
		return merror.Forbidden()
	}

	user, err := am.accounts.Get(ctx, ad.UserID)
	if err != nil {
		return err
	}

	if user.AvatarURI.IsZero() {
		return merror.Conflict().Describe("avatar already deleted")
	}

	ad.Filename = filepath.Base(user.AvatarURI.String)

	err = am.avatars.DeleteAvatar(ad)
	if err != nil {
		return err
	}

	user.AvatarURI = null.NewString("", false)

	return am.accounts.Update(ctx, user, nil)
}

// Partially delete a user
func (am *AuthManager) Delete(ctx context.Context, id string) error {
	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	// Verify authenticated user id and access JWT user id are the same.
	if acc == nil || acc.IsNotUser(id) {
		return merror.Forbidden()
	}

	user, err := am.accounts.Get(ctx, id)
	if err != nil {
		return err
	}

	// check user consents
	// user cannot delete their account if they are still in consent to share data with a third party application
	userConsents, err := am.userConsents.List(ctx, &model.UserConsentFilters{UserID: id})
	if err != nil {
		return merror.Forbidden()
	}

	if len(userConsents) > 0 {
		return merror.Conflict().Describe("user should remove some consents to delete their account")
	}

	// Call application-feedback user dissimulation route
	err = am.feedbacks.Delete(ctx, id)
	if err != nil {
		return err
	}

	// Call databox user's dissimulation route
	err = am.databoxes.Delete(ctx, id)
	if err != nil {
		return err
	}

	// Call consent-backend user's dissimulation route
	err = am.consents.Delete(ctx, id)
	if err != nil {
		return err
	}

	// Creating a user deletion object
	userDeletion := model.UserDeletion{}
	userDeletion.UserID = user.ID
	userDeletion.Notified = false
	err = am.userDeletions.Insert(ctx, &userDeletion)
	if err != nil {
		return err
	}

	// Deleting user's repo
	err = am.accounts.Delete(ctx, user)
	if err != nil {
		return err
	}

	go func() {
		err = am.notifyDeletion(ctx, user.Email)
		if err != nil {
			logger.FromCtx(ctx).Warn().Err(err).
				Msgf("could not send account deletion confirmation email to %s", user.Email)
		}
	}()

	return nil
}
