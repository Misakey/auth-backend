package service

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/auth"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/adaptor/slices"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/auth-backend/src/service/security"
)

// LoginInfo initiates a user credential's check
// It interacts with hydra to know either user is already authenticated or not
// It returns a URL user's agent should be redirect to
func (am *AuthManager) LoginInfo(ctx context.Context, challenge string) string {
	// get info about current login request
	logInfo, err := am.sessions.LoginInfo(ctx, challenge)
	if err != nil {
		return auth.BuildRedirectErr(merror.InvalidFlowCode, err.Error(), am.loginURL)
	}

	redirectTo := am.loginURL
	// if skip is true the user is authenticated
	// in this case we directly inform hydra about it
	if logInfo.Skip {
		accReq := model.LoginAcceptRequest{
			Subject: logInfo.Subject,
		}
		accInfo, err := am.sessions.LoginAccept(ctx, challenge, accReq)
		if err != nil {
			urlWithCode, err := merror.AddCodeToURL(am.loginURL, merror.InvalidFlowCode)
			if err != nil {
				return auth.BuildRedirectErr(merror.InternalCode, err.Error(), am.loginURL)
			}
			accInfo.RedirectTo = urlWithCode
		}

		// return redirect url with login verifier
		redirectTo = accInfo.RedirectTo
	}

	// add login_challenge to be able to receive it on /login/authenticate request
	query := url.Values{}
	query.Set("login_challenge", challenge)

	// add hydra info for client in request
	query.Set("client_name", logInfo.Info.AppName)
	query.Set("client_id", logInfo.Info.ClientID)
	query.Set("logo_uri", logInfo.Info.LogoURI)

	// return an url to authenticate an user
	return fmt.Sprintf("%s?%s", redirectTo, query.Encode())
}

// Authenticate verifies user's credentials are valid and notify hydra about it
func (am *AuthManager) Authenticate(ctx context.Context, auth model.AuthRequest) (*model.LoginAcceptInfo, error) {
	// get info about current login request
	logInfo, err := am.sessions.LoginInfo(ctx, auth.Challenge)
	if err != nil {
		return nil, merror.Forbidden().Describe(err.Error()).Detail("challenge", merror.DVExpired)
	}

	// retrieve user using email
	user, err := am.accounts.GetByEmail(ctx, auth.Email, true)
	if err != nil {
		return nil, merror.Transform(err).From(merror.OriBody)
	}

	// check hashed password is the same
	b, err := security.CheckHashedPassword(auth.Password, user.Password)
	if err != nil || !b {
		return nil, merror.Forbidden().Describe(err.Error()).From(merror.OriBody).Detail("password", merror.DVInvalid)
	}

	// if user will be deleted, return an error
	if user.ToDelete {
		deletionDate := user.UpdatedAt.Add(am.timeBeforeDeletion)
		return nil, merror.Conflict().
			Describe("account will be deleted").
			Detail("to_delete", merror.DVConflict).
			Detail("deletion_date", deletionDate.Format(time.RFC3339))
	}

	// check user is confirmed
	if !user.Confirmed {
		return nil, merror.Conflict().Describe("account has not been confirmed").Detail("confirmed", merror.DVConflict)
	}

	// check all authentication scopes are allowed (only misadmin + roles, some scopes checks being done during the consent flow)
	err = am.checkAuthNScopes(ctx, logInfo.Info.ClientID, user.ID, user.Email, logInfo.RequestedScope)
	if err != nil {
		return nil, err
	}

	// finally accepts login if everything went well
	accReq := model.LoginAcceptRequest{
		Acr:         "0",
		Remember:    true,
		RememberFor: user.KeepAliveDuration,
		Subject:     user.ID,
	}
	accInfo, err := am.sessions.LoginAccept(ctx, auth.Challenge, accReq)
	if err != nil {
		urlWithCode, err := merror.AddCodeToURL(am.loginURL, merror.InvalidFlowCode)
		if err != nil {
			return nil, err
		}
		accInfo.RedirectTo = urlWithCode
	}

	// initiate login flow by redirecting to hydra
	return &accInfo, nil
}

// Logout : Logout the user and expired his session
func (am *AuthManager) Logout(ctx context.Context, userID string) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	// verify authenticated user id and requested user id to logout are the same.
	if acc == nil || acc.IsNotUser(userID) {
		return merror.Forbidden().Describe("missing user scope or wrong subject")
	}

	// handle token revocation / end of session only for our clients
	revocation := model.TokenRevocation{
		Token: acc.Token,
	}
	switch acc.ClientID {
	case am.appCreds.ClientID:
		revocation.Credentials = am.appCreds
	case am.misadminCreds.ClientID:
		revocation.Credentials = am.misadminCreds
	default:
		return merror.Forbidden().Describe("cannot expire token of third party app").Detail("client_id", merror.DVForbidden)
	}

	err := am.sessions.Logout(ctx, userID)
	if err != nil {
		return err
	}

	err = am.sessions.RevokeToken(ctx, revocation)
	if err != nil {
		return err
	}
	return nil
}

// checkAuthNScopes controls authentication scopes format & eligibility, about roles.
// 1. if misadmin scope is involved:
// 				- it checks the user is an allowed misadmin
// 2. if a role scope exists, does the user linked to the auth request own the requested role ?
func (am *AuthManager) checkAuthNScopes(
	ctx context.Context,
	clientID string, userID string, userEmail string,
	scopes []string,
) error {
	// NOTE: hardcode possible misadmins
	misadminEmails := []string{
		"antoine.vadot@misakey.com",
		"arthur.blanchon@misakey.com",
		"nicolas.sadin@misakey.com",
	}

	roleAlreadyMet := false
	// check misadmin & role scopes
	for _, sco := range scopes {
		if !ajwt.IsAllowedScope(sco) {
			return merror.Forbidden().Describef("unknown scope %s", sco).Detail("scope", merror.DVForbidden)
		}
		// for misadmin scope: allow only for misadmin client and for harcoded user emails
		if ajwt.IsMisadminScope(sco) {
			if !slices.ContainsString(misadminEmails, userEmail) {
				return merror.Forbidden().Describef("invalid misadmin user %s", userEmail).Detail("scope", merror.DVForbidden)
			}
			continue
		}

		// for role scope: allow only one role and check user owns the role
		if ajwt.IsRoleScope(sco) {
			// only one role can be asked for a given auth session
			if roleAlreadyMet {
				return merror.Forbidden().Describe("can only request one role").Detail("scope", merror.DVInvalid)
			}
			// directly act the fact we met already one role scope
			roleAlreadyMet = true
			err := am.checkRoleOwnership(ctx, userID, sco)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (am *AuthManager) checkRoleOwnership(ctx context.Context, userID string, sco string) error {
	// retrieve role info from scope
	appRole := ajwt.GetRole(sco)
	// build filters & verify user owns the role
	filters := model.UserRoleFilters{
		UserID:        userID,
		ApplicationID: appRole.ApplicationID,
		RoleLabel:     appRole.RoleLabel,
	}
	// get user roles using filters
	userRoles, err := am.userRoles.List(ctx, filters)
	if err != nil {
		return err
	}
	// if the returned list is empty, the user doesn't own the role
	if len(userRoles) == 0 {
		return merror.Forbidden().Describef("forbidden %s", sco).Detail("scope", merror.DVForbidden)
	}
	return nil
}
