package service

import (
	"regexp"
	"time"

	"gitlab.com/Misakey/auth-backend/src/model"
)

type AuthManager struct {
	//
	// Resource repositories section
	accounts   userAccountRepo
	avatars    avatarRepo
	backups    backupRepo
	publicKeys publicKeyRepo

	sessions sessionRepo
	tokens   tokenRepo

	customAccesses customAccessRepo
	userRoles      userRoleRepo

	userConsents userConsentRepo

	userDeletions userDeletionRepo

	// Service repositories section
	feedbacks feedbackRepo
	databoxes databoxRepo
	consents  consentRepo

	//
	// Emails configuration
	emailRenderer renderer
	mailer        mailerRepo

	//
	// ID of Misakey clients
	appCreds      model.Credentials
	misadminCreds model.Credentials

	// login service
	// time before definitely deleting resources
	timeBeforeDeletion time.Duration
	// the URL user's agent should redirect to to perform authentication
	loginURL string

	// user account service
	userHandleRegexp *regexp.Regexp
}

func NewAuthManager(
	emailRenderer renderer, mailer mailerRepo,
	accounts userAccountRepo, avatars avatarRepo, backups backupRepo, publicKeys publicKeyRepo,
	sessions sessionRepo, tokens tokenRepo,
	customAccesses customAccessRepo, userRoles userRoleRepo, userConsents userConsentRepo,
	feedbacks feedbackRepo, databoxes databoxRepo, consents consentRepo,
	userDeletions userDeletionRepo,
	loginURL string,
	mkAppID string, mkAppSecret string,
	misadminID string, misadminSecret string,
	timeBeforeDeletion time.Duration,
) *AuthManager {
	return &AuthManager{
		emailRenderer: emailRenderer,
		mailer:        mailer,

		accounts:   accounts,
		avatars:    avatars,
		backups:    backups,
		publicKeys: publicKeys,

		sessions: sessions,
		tokens:   tokens,

		customAccesses: customAccesses,
		userRoles:      userRoles,

		userConsents: userConsents,

		userDeletions: userDeletions,

		feedbacks: feedbacks,
		databoxes: databoxes,
		consents:  consents,

		appCreds:      model.Credentials{ClientID: mkAppID, ClientSecret: mkAppSecret},
		misadminCreds: model.Credentials{ClientID: misadminID, ClientSecret: misadminSecret},

		loginURL: loginURL,

		timeBeforeDeletion: timeBeforeDeletion,

		userHandleRegexp: regexp.MustCompile(`^[a-z0-9][a-z0-9_]{1,19}[a-z0-9]$`),
	}
}
