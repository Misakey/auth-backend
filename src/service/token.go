package service

import (
	"context"
	"time"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/auth-backend/src/service/security"
)

const (
	accessTokenType      = "access_token"
	customTokenSize      = 32
	customAccessDuration = time.Hour * 24
)

// Introspect opaque access token, valid it & return it as JWT with instropected values
// it uses auth server since it handles tokens generated by it
func (am *AuthManager) GetAccessJWT(ctx context.Context, opaqueTok string) (string, error) {
	introTok, err := am.tokens.Introspect(ctx, opaqueTok)
	if err != nil {
		return "", merror.Unauthorized().Describe(err.Error())
	}

	// check access token is active
	// see https://www.ory.sh/docs/hydra/sdk/api#oauth2tokenintrospection to know what is an active token
	if !introTok.Active {
		return "", merror.Unauthorized().Describe("token is not active")
	}

	// check token is an access_token
	if introTok.TokenType != accessTokenType {
		return "", merror.Unauthorized().Describe("token must be an access token")
	}

	// format introspect token as a jwt string
	ac := ajwt.AccessClaims{
		Audiences: []string{introTok.ClientID}, // the one audience if the party to which to token was issued
		ExpiresAt: introTok.ExpiresAt,
		IssuedAt:  introTok.IssuedAt,
		Issuer:    introTok.Issuer,
		NotBefore: introTok.NotBefore,
		Subject:   introTok.Subject,
		Scope:     introTok.Scope,
		ClientID:  introTok.ClientID,
		Token:     opaqueTok,
	}

	if ac.Subject == "" {
		return "", merror.Unauthorized().Describe("subject is empty")
	}

	return ajwt.GetSignedToken(&ac)
}

// Introspect opaque access token, valid it & return it as JWT with instropected values
// it uses internal custom token database since it handles tokens generated by it
// it also handles classic token is custom token returned a not found error
func (am *AuthManager) GetAccessJWTCustom(ctx context.Context, opaqueTok string) (string, error) {
	customAccess, err := am.customAccesses.Get(ctx, opaqueTok)
	if err != nil && merror.HasCode(err, merror.NotFoundCode) {
		// if not found, try to handle the token the classic way
		return am.GetAccessJWT(ctx, opaqueTok)
	} else if err != nil {
		return "", merror.Unauthorized().Describe(err.Error())
	}

	// compute expiration date of the token
	customAccess.ExpiresAt = customAccess.CreatedAt.Add(customAccessDuration)
	// check access token is active
	if customAccess.ExpiresAt.Before(time.Now()) {
		return "", merror.Unauthorized().Describe("token is not active")
	}

	// format introspect token as a jwt string
	ac := ajwt.AccessClaims{
		ExpiresAt: customAccess.ExpiresAt.Unix(),
		IssuedAt:  customAccess.CreatedAt.Unix(),
		NotBefore: customAccess.CreatedAt.Unix(),
		Subject:   customAccess.Subject,
		Scope:     customAccess.Scope,
		Issuer:    "auth.misakey.com",
		ClientID:  am.appCreds.ClientID,
		Token:     opaqueTok,
	}

	return ajwt.GetSignedToken(&ac)
}

// CreateCustomAccess creates a custom access
func (am *AuthManager) CreateCustomAccess(ctx context.Context, ca *model.CustomAccess) error {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAnyService() {
		return merror.Forbidden().Describe("missing service scope")
	}

	// generate a token
	token, err := security.RandomAccessToken(customTokenSize)
	if err != nil {
		return err
	}

	ca.Token = string(token)
	// create token
	err = am.customAccesses.Insert(ctx, ca)
	if err != nil {
		return err
	}

	// compute expiry time
	ca.ExpiresAt = ca.CreatedAt.Add(customAccessDuration)
	return nil
}
