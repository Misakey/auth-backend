package service

import (
	"context"

	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// Create
func (am *AuthManager) CreateRole(ctx context.Context, userRole *model.UserRole) error {
	// grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAnyService() && acc.IsNotAdminOn(userRole.ApplicationID) {
		return merror.Forbidden().Describe("missing service scope or admin role")
	}

	return am.userRoles.Insert(ctx, userRole)
}

// ListRoles
func (am *AuthManager) ListRoles(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(filters.UserID) {
		return nil, merror.Forbidden()
	}

	return am.userRoles.List(ctx, filters)
}
