package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/auth-backend/src/model"
)

// Mocks

type sessionsMock struct {
	logout func(context.Context, string) error
}

func (m sessionsMock) LoginInfo(ctx context.Context, challenge string) (*model.LoginInfo, error) {
	return nil, nil
}
func (m sessionsMock) LoginAccept(ctx context.Context, challenge string, acceptReq model.LoginAcceptRequest) (model.LoginAcceptInfo, error) {
	return model.LoginAcceptInfo{}, nil
}

func (m sessionsMock) Logout(ctx context.Context, id string) error {
	return m.logout(ctx, id)
}

func (m sessionsMock) RevokeToken(ctx context.Context, revocation model.TokenRevocation) error {
	return nil
}

// Tests

//
func TestLogout(t *testing.T) {
	tests := map[string]struct {
		acc           *ajwt.AccessClaims
		userID        string
		sessionLogout func(context.Context, string) error
		expectedErr   error
	}{
		"a user without jwt logout their account - forbidden": {
			userID:      "1",
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope logout their account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			userID:      "1",
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope logout another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			userID:      "2",
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope logout another user account - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			userID:      "2",
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope logout their account - allowed": {
			acc:    &ajwt.AccessClaims{Subject: "42", Scope: "user", Token: "raw_token"},
			userID: "42",
			sessionLogout: func(_ context.Context, userID string) error {
				assert.Equal(t, "42", userID)
				return nil
			},
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)
			// init service with mocked sub-layers
			service := AuthManager{
				sessions: &sessionsMock{logout: test.sessionLogout},
			}

			// call function to test
			err := service.Logout(ctx, test.userID)

			// check assertions
			if test.expectedErr != nil {
				assert.Equal(t, test.expectedErr.Error(), err.Error())
			}
		})

	}
}

// Tests

//
func TestCheckAuthNScopes(t *testing.T) {
	tests := map[string]struct {
		clientID      string
		userID        string
		userEmail     string
		scopes        []string
		userRolesList func(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error)
		assert        func(*testing.T, error)
	}{
		"unknown scope should not pass": {
			scopes: []string{"openid", "user", "bamba_triste"},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.ForbiddenCode, merror.Transform(err).Co)
				assert.Equal(t, "unknown scope bamba_triste", merror.Transform(err).Desc)
			},
		},
		"misadmin scope for unknown misadmin user should not pass": {
			clientID:  "misadmin-uuid",
			userEmail: "jean@valjean.io",
			scopes:    []string{"openid", "user", "misadmin"},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.ForbiddenCode, merror.Transform(err).Co)
				assert.Equal(t, "invalid misadmin user jean@valjean.io", merror.Transform(err).Desc)
			},
		},
		"misadmin scope/client/user that should pass": {
			clientID:  "misadmin-uuid",
			userEmail: "nicolas.sadin@misakey.com",
			scopes:    []string{"openid", "user", "misadmin"},
			assert: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
		"many role scopes should not pass": {
			clientID: "mk-app-uuiid",
			userID:   "18b88d48-ad48-43b9-a323-eab1de68b280",
			scopes: []string{"openid", "user",
				"rol.admin.00000000-1111-2222-3333-444444444444",
				"rol.dpo.00000000-1111-2222-3333-444444444444",
			},
			userRolesList: func(_ context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
				assert.Equal(t, "00000000-1111-2222-3333-444444444444", filters.ApplicationID)
				assert.Equal(t, "18b88d48-ad48-43b9-a323-eab1de68b280", filters.UserID)
				assert.Equal(t, "admin", filters.RoleLabel)
				return []*model.UserRole{&model.UserRole{ID: 1}}, nil
			},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.ForbiddenCode, merror.Transform(err).Co)
				assert.Equal(t, "can only request one role", merror.Transform(err).Desc)
			},
		},
		"role scope not owned by user should not pass": {
			clientID: "mk-app-uuiid",
			userID:   "18b88d48-ad48-43b9-a323-eab1de68b280",
			scopes:   []string{"openid", "user", "rol.dpo.00000000-1111-2222-3333-444444444444"},
			userRolesList: func(_ context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
				assert.Equal(t, "00000000-1111-2222-3333-444444444444", filters.ApplicationID)
				assert.Equal(t, "18b88d48-ad48-43b9-a323-eab1de68b280", filters.UserID)
				assert.Equal(t, "dpo", filters.RoleLabel)
				// return empty string -> not owned
				return []*model.UserRole{}, nil
			},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.ForbiddenCode, merror.Transform(err).Co)
				assert.Equal(t, "forbidden rol.dpo.00000000-1111-2222-3333-444444444444", merror.Transform(err).Desc)
			},
		},
		"role scope owned by user should pass": {
			clientID: "mk-app-uuiid",
			userID:   "18b88d48-ad48-43b9-a323-eab1de68b280",
			scopes:   []string{"openid", "user", "rol.admin.00000000-1111-2222-3333-444444444444"},
			userRolesList: func(_ context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
				assert.Equal(t, "00000000-1111-2222-3333-444444444444", filters.ApplicationID)
				assert.Equal(t, "18b88d48-ad48-43b9-a323-eab1de68b280", filters.UserID)
				assert.Equal(t, "admin", filters.RoleLabel)
				return []*model.UserRole{&model.UserRole{ID: 1}}, nil
			},
			assert: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
	}

	for description, test := range tests {
		t.Run(description, func(t *testing.T) {
			// init service with mocked sub-layers
			service := AuthManager{
				userRoles: &userRoleMock{list: test.userRolesList},
			}

			// call function to test
			err := service.checkAuthNScopes(context.Background(), test.clientID, test.userID, test.userEmail, test.scopes)

			test.assert(t, err)
		})
	}
}
