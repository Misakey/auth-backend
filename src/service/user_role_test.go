package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

// Mocks

type userRoleMock struct {
	insert func(ctx context.Context, userRole *model.UserRole) error
	list   func(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error)
}

func (m userRoleMock) Insert(ctx context.Context, userRole *model.UserRole) error {
	return m.insert(ctx, userRole)
}

func (m userRoleMock) List(ctx context.Context, filters model.UserRoleFilters) ([]*model.UserRole, error) {
	return m.list(ctx, filters)
}

// Tests

//
func TestCreateRole(t *testing.T) {
	tests := map[string]struct {
		acc      *ajwt.AccessClaims
		userRole *model.UserRole
		insert   func(context.Context, *model.UserRole) error
		assert   func(*testing.T, error)
	}{
		"a call without jwt cannot create a role": {
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.Forbidden().Error(), err.Error())
			},
		},
		"a call with user scope cannot create a role": {
			acc:      &ajwt.AccessClaims{Scope: "user"},
			userRole: &model.UserRole{},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.Forbidden().Error(), err.Error())
			},
		},
		"a call with application scope cannot create a role": {
			acc:      &ajwt.AccessClaims{Scope: "application"},
			userRole: &model.UserRole{},
			assert: func(t *testing.T, err error) {
				assert.Equal(t, merror.Forbidden().Error(), err.Error())
			},
		},
		"a call with service scope can create a role": {
			acc:      &ajwt.AccessClaims{Scope: "service"},
			userRole: &model.UserRole{RoleLabel: "admin", ApplicationID: "misakey"},
			insert: func(_ context.Context, ur *model.UserRole) error {
				assert.Equal(t, "admin", ur.RoleLabel)
				assert.Equal(t, "misakey", ur.ApplicationID)
				return nil
			},
			assert: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
		"a call with user & admin scope can create a role": {
			acc:      &ajwt.AccessClaims{Scope: "user rol.admin.00000000-1111-2222-3333-444444444444"},
			userRole: &model.UserRole{RoleLabel: "admin", ApplicationID: "00000000-1111-2222-3333-444444444444"},
			insert: func(_ context.Context, ur *model.UserRole) error {
				assert.Equal(t, "admin", ur.RoleLabel)
				assert.Equal(t, "00000000-1111-2222-3333-444444444444", ur.ApplicationID)
				return nil
			},
			assert: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
	}

	for description, test := range tests {
		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)
			// init service with mocked sub-layers
			service := AuthManager{
				userRoles: &userRoleMock{insert: test.insert},
			}

			// call function to test
			err := service.CreateRole(ctx, test.userRole)

			test.assert(t, err)
		})
	}
}

func TestListRoles(t *testing.T) {
	tests := map[string]struct {
		acc           *ajwt.AccessClaims
		filters       model.UserRoleFilters
		roleList      func(_ context.Context, _ model.UserRoleFilters) ([]*model.UserRole, error)
		expectedUsers []*model.UserRole
		expectedErr   error
	}{
		"a call without jwt cannot list  user roles": {
			filters:     model.UserRoleFilters{UserID: "uuid_1"},
			expectedErr: merror.Forbidden(),
		},
		"a user cannot list another user's roles": {
			acc:         &ajwt.AccessClaims{Subject: "uuid_1", Scope: "user"},
			filters:     model.UserRoleFilters{UserID: "uuid_2"},
			expectedErr: merror.Forbidden(),
		},
		"a user can request their roles": {
			acc:     &ajwt.AccessClaims{Subject: "uuid_1", Scope: "user"},
			filters: model.UserRoleFilters{UserID: "uuid_1"},
			roleList: func(_ context.Context, filter model.UserRoleFilters) ([]*model.UserRole, error) {
				assert.Equalf(t, "uuid_1", filter.UserID, "list roles")
				return []*model.UserRole{&model.UserRole{ID: 1, UserID: "uuid_1", RoleLabel: "admin", ApplicationID: "app_uuid_1"}}, nil
			},
			expectedUsers: []*model.UserRole{&model.UserRole{ID: 1, UserID: "uuid_1", RoleLabel: "admin", ApplicationID: "app_uuid_1"}},
			expectedErr:   nil,
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{
				userRoles: &userRoleMock{list: test.roleList},
			}

			// call function to test
			users, err := service.ListRoles(ctx, test.filters)

			// check assertions
			assert.Equal(t, test.expectedUsers, users)
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
