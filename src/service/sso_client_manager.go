package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
	"gitlab.com/Misakey/msk-sdk-go/patch"

	"gitlab.com/Misakey/auth-backend/src/adaptor/slices"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/auth-backend/src/service/security"
)

const (
	clientSecretSize = 21
)

// SSOClientManager service handles Creation and Update of Hydra SSO Clients
type SSOClientManager struct {
	ssoClients   ssoClientRepo
	applications applicationRepo

	applicationClientID string
	misadminClientID    string
}

func NewSSOClientManager(ssoClients ssoClientRepo, applications applicationRepo,
	applicationClientID, misadminClientID string) *SSOClientManager {
	return &SSOClientManager{
		ssoClients:   ssoClients,
		applications: applications,

		applicationClientID: applicationClientID,
		misadminClientID:    misadminClientID,
	}
}

// Create
func (sc *SSOClientManager) Create(ctx context.Context, hydraClient *model.HydraClient) error {
	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)

	// Verify authenticated user id and access JWT user id are the same.
	if acc == nil || acc.IsNotAdminOn(hydraClient.ID) {
		return merror.Forbidden().Describe("missing user scope or admin role")
	}

	appInfo, err := sc.applications.Get(ctx, hydraClient.ID)
	if err != nil {
		return err
	}

	hydraClient.Name = appInfo.Name
	hydraClient.LogoURI = appInfo.LogoURI
	hydraClient.Scope = "openid user application pur.*"
	hydraClient.Audience = []string{hydraClient.ID}
	hydraClient.GrantTypes = []string{"authorization_code", "client_credentials"}
	hydraClient.ResponseTypes = []string{"code", "token"}
	hydraClient.SubjectType = "pairwise"
	hydraClient.UserinfoSignedResponseALG = "none"
	hydraClient.TokenEndpointAuthMethod = "client_secret_post"
	hydraClient.Secret, err = security.NewSecret(clientSecretSize)
	hydraClient.SecretExpiresAt = 0
	if err != nil {
		return merror.Internal().Describe("unable to generate new secret")
	}

	ssoClient, err := sc.ssoClients.GetClient(ctx, hydraClient.ID)
	if ssoClient != nil && err == nil {
		return merror.Conflict().Describe("already exists")
	}

	// ignore not found error since it is an expected behaviour - we want to create it
	if err != nil && !merror.HasCode(err, merror.NotFoundCode) {
		return err
	}
	return sc.ssoClients.CreateClient(ctx, hydraClient)
}

// PartialUpdate
func (sc *SSOClientManager) PartialUpdate(ctx context.Context, pi *patch.Info) error {
	patchedSSOClient := pi.Model.(*model.HydraClientPatch)

	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// check if the connected user is `admin` on the requested app or user
	if acc.IsNotAdminOn(patchedSSOClient.ID) && acc.IsNotAnyUser() {
		return merror.Forbidden().Describe("missing admin or user role")
	}

	ssoClient, err := sc.ssoClients.GetClient(ctx, patchedSSOClient.ID)
	if err != nil {
		return err
	}

	if slices.ContainsString(pi.Input, "RedirectURIs") {
		ssoClient.RedirectURIs = patchedSSOClient.RedirectURIs
	}
	if slices.ContainsString(pi.Input, "AllowedCorsOrigins") {
		ssoClient.AllowedCorsOrigins = patchedSSOClient.AllowedCorsOrigins
	}

	return sc.ssoClients.UpdateClient(ctx, ssoClient)
}

// GenerateSecret
func (sc *SSOClientManager) GenerateSecret(ctx context.Context, appID string) (string, error) {
	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	// Verify authenticated user id and access JWT user id are the same.
	if acc == nil || acc.IsNotAdminOn(appID) {
		return "", merror.Forbidden().Describe("missing user scope or admin role")
	}

	// Block Misakey / Misadmin / Admin app
	if appID == sc.applicationClientID ||
		appID == sc.misadminClientID {
		return "", merror.Forbidden().Describef("can't regenerate secret for %s", appID)
	}

	hydraClient, err := sc.ssoClients.GetClient(ctx, appID)
	if err != nil {
		return "", err
	}

	hydraClient.Secret, err = security.NewSecret(clientSecretSize)
	if err != nil {
		return "", err
	}

	err = sc.ssoClients.UpdateClient(ctx, hydraClient)
	if err != nil {
		return "", err
	}

	return hydraClient.Secret, err
}

// Get
func (sc *SSOClientManager) Get(ctx context.Context, appID string) (*model.HydraClient, error) {
	// Grab accesses from context
	acc := ajwt.GetAccesses(ctx)
	// Verify authenticated user id and access JWT user id are the same.
	if acc == nil || acc.IsNotAdminOn(appID) {
		return nil, merror.Forbidden()
	}

	return sc.ssoClients.GetClient(ctx, appID)
}
