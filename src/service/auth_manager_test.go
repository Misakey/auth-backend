package service

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewAuthManager(t *testing.T) {
	timeBeforeDeletion, _ := time.ParseDuration("360h")
	am := NewAuthManager(
		nil, nil,
		nil, nil, nil, nil,
		nil, nil,
		nil, nil, nil,
		nil, nil, nil, nil,
		"login_url",
		"app-id", "app-secret",
		"misadmin-id", "misadmin-secret",
		timeBeforeDeletion,
	)
	assert.Equal(t, "login_url", am.loginURL)
	assert.Equal(t, "app-id", am.appCreds.ClientID)
	assert.Equal(t, "app-secret", am.appCreds.ClientSecret)
	assert.Equal(t, "misadmin-id", am.misadminCreds.ClientID)
	assert.Equal(t, "misadmin-secret", am.misadminCreds.ClientSecret)
	assert.Equal(t, 15*24.0, am.timeBeforeDeletion.Hours())
}
