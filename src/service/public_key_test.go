package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

//
func TestAddPublicKey(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		pubKey      *model.PublicKey
		expectedErr error
	}{
		"a user without jwt upload public key - forbidden": {
			pubKey:      &model.PublicKey{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope upload their public key - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pubKey:      &model.PublicKey{UserID: "1"},
			expectedErr: merror.Forbidden(),
		},
		"a user without user scope upload another user public key - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			pubKey:      &model.PublicKey{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
		"a user with user scope upload another user public key - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			pubKey:      &model.PublicKey{UserID: "2"},
			expectedErr: merror.Forbidden(),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.AddPublicKey(ctx, test.pubKey)

			// check assertions
			assert.Equal(t, test.expectedErr.Error(), err.Error())
		})

	}
}
