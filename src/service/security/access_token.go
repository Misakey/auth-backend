package security

import (
	"crypto/rand"
	"encoding/base64"
	"io"

	"github.com/pkg/errors"
)

// RandomAccessToken a string of size n by reading from crypto/rand.Reader
func RandomAccessToken(n uint) (string, error) {
	bytes := make([]byte, n)
	if _, err := io.ReadFull(rand.Reader, bytes); err != nil {
		return "", errors.WithStack(err)
	}
	return base64.StdEncoding.EncodeToString(bytes), nil
}
