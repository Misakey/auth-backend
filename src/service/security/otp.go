package security

import (
	"crypto/rand"
	"io"
)

const (
	otpSize = 6
)

// GenerateOTP : Generate a cryptographically secure pseudorandom number generator.
func GenerateOTP() (string, error) {
	b := make([]byte, otpSize)
	n, err := io.ReadAtLeast(rand.Reader, b, otpSize)
	if n != otpSize {
		return "", err
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b), nil
}

var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

// HashOTP : Generate a hash from argon2 algo
func HashOTP(otp string) (string, error) {

	encodedHashOtp, err := generateFromPassword(otp)
	if err != nil {
		return "", err
	}

	return encodedHashOtp, nil
}

// CheckHashPassword use argon2 and compare function to check input hashed
// password and source are the same
func CheckHashedOTP(input string, source string) (bool, error) {
	match, err := comparePasswordAndHash(input, source)
	if err != nil {
		return false, err
	}
	if !match {
		return false, nil
	}
	return true, nil
}
