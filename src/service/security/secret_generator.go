// Copy paste from security/access_token.
// used for more consistent function name

package security

import (
	"crypto/rand"
	"encoding/base64"
	"io"

	"github.com/pkg/errors"
)

// NewSecret generate a random string of size n by reading from crypto/rand.Reader
func NewSecret(n uint) (string, error) {
	bytes := make([]byte, n)
	if _, err := io.ReadFull(rand.Reader, bytes); err != nil {
		return "", errors.WithStack(err)
	}
	return base64.StdEncoding.EncodeToString(bytes), nil
}
