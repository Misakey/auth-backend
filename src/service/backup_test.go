package service

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/auth-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

//
func TestAddBackup(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		backup      *model.Backup
		expectedErr error
	}{
		"a user without jwt upload backup - forbidden": {
			backup:      &model.Backup{UserID: "1"},
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope upload their backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			backup:      &model.Backup{UserID: "1"},
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope upload another user backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			backup:      &model.Backup{UserID: "2"},
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user with user scope upload another user backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			backup:      &model.Backup{UserID: "2"},
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			err := service.AddBackup(ctx, test.backup)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}

//
func TestRetrieveBackup(t *testing.T) {
	tests := map[string]struct {
		acc         *ajwt.AccessClaims
		id          string
		expectedErr error
	}{
		"a user without jwt upload backup - forbidden": {
			id:          "1",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope upload their backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "1",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user without user scope upload another user backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1"},
			id:          "2",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
		"a user with user scope upload another user backup - forbidden": {
			acc:         &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			id:          "2",
			expectedErr: merror.Forbidden().Describe("missing user scope or wrong subject"),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init test data
			ctx := ajwt.SetAccesses(context.Background(), test.acc)

			// init service with mocked sub-layers
			service := AuthManager{}

			// call function to test
			_, err := service.RetrieveBackup(ctx, test.id)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
