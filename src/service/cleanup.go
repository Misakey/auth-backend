package service

import (
	"context"
	"time"

	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/auth-backend/src/model"
)

func (cm *CleanupManager) CleanupAccounts(ctx context.Context) error {
	logger.FromCtx(ctx).Info().Msg("starting cleanup job")

	// set durations that will be used in filters
	reminderDuration, _ := time.ParseDuration("48h")
	softDeleteBefore := time.Now().Add(-cm.timeBeforeDeletion)
	reminderBefore := softDeleteBefore.Add(reminderDuration)

	// remind users of imminent account deletion
	remindFilters := model.UserDeletionFilters{}
	remindFilters.Notified.SetValid(false)
	remindFilters.CreatedBefore = reminderBefore

	usersToRemind, err := cm.userDeletions.List(ctx, remindFilters)
	if err != nil {
		return err
	}

	if usersToRemind == nil || len(usersToRemind) == 0 {
		logger.FromCtx(ctx).Info().
			Msg("no account to notify")
	} else {
		if err := cm.remindDeletion(ctx, usersToRemind); err != nil {
			return err
		}
	}

	// hard delete user accounts
	deleteFilters := model.UserDeletionFilters{}
	deleteFilters.CreatedBefore = softDeleteBefore
	deleteFilters.Notified.SetValid(true)
	usersToHardDelete, err := cm.userDeletions.List(ctx, deleteFilters)
	if err != nil {
		return err
	}

	if usersToHardDelete == nil || len(usersToHardDelete) == 0 {
		logger.FromCtx(ctx).Info().
			Msg("no account to hard delete")
	} else {
		userIDs := []string{}
		for _, user := range usersToHardDelete {
			logger.FromCtx(ctx).Info().
				Msgf("planning %s deletion", user.UserID)
			userIDs = append(userIDs, user.UserID)
		}
		if err := cm.hardDelete(ctx, userIDs); err != nil {
			return err
		}
	}

	return nil
}

// remindDeletion notifies users who will have their account deleted in less than 48h
func (cm *CleanupManager) remindDeletion(ctx context.Context, users []*model.UserDeletion) error {
	for _, user := range users {
		logger.FromCtx(ctx).Info().
			Msgf("sending notification to %s", user.UserID)
		if err := cm.notifyDeletionReminder(ctx, user.UserAccount.Email); err != nil {
			logger.FromCtx(ctx).Warn().Err(err).
				Msgf("could not send account deletion reminder email to %s", user.UserAccount.Email)
		} else {
			user.Notified = true
			if err := cm.userDeletions.Update(ctx, user); err != nil {
				return err
			}
		}
	}
	return nil
}

// hardDelete permanently deletes user accounts
func (cm *CleanupManager) hardDelete(ctx context.Context, userIDs []string) error {
	return cm.accounts.BulkHardDelete(ctx, userIDs)
}

// notifyDeletionReminder notifies a user that their account will soon be deleted
func (cm *CleanupManager) notifyDeletionReminder(ctx context.Context, to string) error {
	data := map[string]interface{}{
		"to": to,
	}
	email, err := cm.emailRenderer.NewEmail(ctx, to, "Rappel - Votre suppression de compte - Misakey", "deletion_reminder", data)
	if err != nil {
		return err
	}
	return cm.mailer.Send(ctx, email)
}
